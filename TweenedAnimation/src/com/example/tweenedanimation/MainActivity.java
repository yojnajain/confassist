package com.example.tweenedanimation;

import android.app.Activity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
	ImageView im=(ImageView)findViewById(R.id.imageView1);
			
	Animation an=AnimationUtils.loadAnimation(this, R.anim.spin);
	
	im.startAnimation(an);
	}

}
