﻿ConfAssist
=============
 If you are planning a conference on a subject having talks, speaker, etc, 
 how do you distribute it’s schedule with attendees? Printed one pager can 
 help by listing down details about talk, speaker and schedule. This 
 following proposal talks about an mobile app which you can share with your 
 attendees or participants and they have conference details always handy on 
 the android phones.
 
Commands
===========
When you change `OnCreate()`, you need to clear database files or application cache. Run the following command to clear app data cache.
```
adb shell pm clear in.enroller.
```
ignore
last ignore
