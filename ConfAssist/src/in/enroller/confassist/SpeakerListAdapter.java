package in.enroller.confassist;

import in.enroller.confassist.model.People;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpeakerListAdapter extends ArrayAdapter<People> {

	TextView speakerName, tagLine, desig;
	ImageView speakerPhoto;
	
	
	public  SpeakerListAdapter(Context context, List<People> people) {
		super(context, R.layout.custom_list,people); 
	}
	
	


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		if(convertView == null)
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.conf_speaker_list, null);

		speakerName = (TextView)convertView.findViewById(R.id.textView_speakerName);
		tagLine =(TextView)convertView.findViewById(R.id.textView_speakerTagLine);
		desig = (TextView)convertView.findViewById(R.id.textView_speakerDesig);
		speakerPhoto = (ImageView)convertView.findViewById(R.id.imageView_speakerPhoto);
		
		People pl = (People)getItem(position);
		
				
		if(pl.getPhoto()!= null && !pl.getPhoto().trim().equals(""))
		{		
		HomeActivity.imageLoader.DisplayImage(pl.getPhoto(), speakerPhoto);
		}
		else
		{
			speakerPhoto.setImageResource(R.drawable.speaker_icon)	;
		}
			
		speakerName.setText(pl.getSpeakerName());
		tagLine.setText(pl.getTagLine());
		desig.setText(pl.getBio());
		speakerName.setTextColor(Color.BLACK);
		tagLine.setTextColor(Color.BLACK);
		desig.setTextColor(Color.BLACK);
		convertView.setTag(pl);
		
		
		return convertView;
	
	
	}	

}
