package in.enroller.confassist;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.People;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TalkListAdapter extends  ArrayAdapter<Agenda>{

	TextView title,speakerName,time;
	ImageView speaker_icon;
	DatabaseAdapter da;	
	TextView dateSectionHeaderView;
	static SimpleDateFormat sdf;
	String Current,previous;
	People pl;
	Agenda agenda;
	
	
	public TalkListAdapter(Context context, List<Agenda> agenda) {
				super(context, R.layout.item_list, agenda);
	}
	
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		
		
		if(view == null)
			view = LayoutInflater.from(getContext()).inflate(R.layout.item_list, null);
			
		
		title = (TextView)view.findViewById(R.id.textView1);
		speakerName = (TextView)view.findViewById(R.id.textView2);
		time = (TextView)view.findViewById(R.id.textView3);
		speaker_icon=(ImageView)view.findViewById(R.id.speaker_icon);
		dateSectionHeaderView  = (TextView)view.findViewById(R.id.textView4);
		 agenda = (Agenda) getItem(position);
		 
		 List<People> people = agenda.getPeople();
		 int i=0;
		 speakerName.setText("");
		 while(i!=people.size() && i<2 )
		 {
		   pl = people.get(i++);
		
		 
		   if(i==1)
		   {
			   speakerName.setText(pl.getSpeakerName());
		   }
		   else
		   {
			   speakerName.setText(speakerName.getText() + ", "+ pl.getSpeakerName());
			   
		   }
		 }
		 if(i>1)
		  speakerName.setText(speakerName.getText() + "...");
			
		view.setTag(agenda);
		title.setText(agenda.getTitle());
		
		Date dt = null;
		dt= new GregorianCalendar(0,0,0).getTime();
		time.setText(Utilities.sdfTime.format(agenda.getStartTime()));
  
//Current date
// date comes from agenda tabel
 		  Date d = agenda.getStartTime();
		  Current = Utilities.sdfPrintFormat.format(d);
 
		  dateSectionHeaderView.setText(Current);
 
//Previous Date		
 
		  if(position>0)
		  {
		  agenda = getItem(position-1);
		  if ( position > 0 ) {
			  d  = agenda.getStartTime();
       		
			  agenda = getItem(position);
	   		  previous = agenda.getStartTime().toString();	    
	   	  }
		  
 //Condition    
		  if (d.toString() == null || !previous.equals(Current)) {
			  dateSectionHeaderView.setVisibility(View.VISIBLE);
		  } else {
			  dateSectionHeaderView.setVisibility(View.GONE);
		  }
		  }
   		return view;
	}

}

		
