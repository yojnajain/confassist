package in.enroller.confassist;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.jsonserialization.PostAttendyToURL;
import in.enroller.confassist.jsonserialization.PostJsonToURL;
import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.Attendy;
import in.enroller.confassist.model.Conference;
import in.enroller.confassist.model.Feedback;
import in.enroller.confassist.model.People;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseAdapter extends SQLiteOpenHelper {
	
	
	public static final String DB_NAME = "ConfDatabase";
	public static final int DB_VERSION = 1;
	public static SimpleDateFormat formatter;
	
	SQLiteDatabase mdb;
	public DatabaseAdapter(Context context)
	{
		super(context,DB_NAME,null,DB_VERSION);
		
	}

	public long insertData(ContentValues v, String TableName)
	{	
		mdb = this.getWritableDatabase();
		long id = mdb.insert(TableName, null, v);
		mdb.close();
		Log.i("Insertion done", "Insertion done");
		return id;
		
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		final String create_conf_speaker_table = "CREATE TABLE people (_id INTEGER PRIMARY KEY, agenda_id NUMERIC, "
				+ "conf_id NUMERIC,type TEXT, photo TEXT, tag_line TEXT, bio TEXT, name TEXT, serverId TEXT)";
		db.execSQL(create_conf_speaker_table);

		final String create_agenda_table = "CREATE TABLE agenda (_id INTEGER PRIMARY KEY, conf_id NUMERIC, "
				+ "title TEXT, abstract TEXT, venue TEXT, start_time DATE, duration NUMERIC, serverId TEXT)";
		db.execSQL(create_agenda_table);

		final String create_conf_table = "CREATE TABLE conference (_id INTEGER PRIMARY KEY, short_code TEXT, "
				+ "title TEXT, description TEXT, logo TEXT, venue TEXT, longitude TEXT, latitude TEXT, creator TEXT, website TEXT, email TEXT, phone_no NUMBER, "
				+ "start_date DATE, end_date DATE, is_agenda_final BOOLEAN, serverId TEXT, like TEXT)";
		db.execSQL(create_conf_table);
		
		final String feedback_table = "CREATE TABLE feedback (_id INTEGER PRIMARY KEY, userName TEXT, emailId TEXT, feedback TEXT, createdDate DATE, agendaId NUMBER, agendaServerId TEXT, rating NUMBER)";
		db.execSQL(feedback_table);
		
		final String attendy_table = "CREATE TABLE attendy (_id INTEGER PRIMARY KEY, confServerId TEXT, confId NUMBER, emailId TEXT, attending TEXT, createdDate Date)";
		db.execSQL(attendy_table); 
		Log.i("DB", "5 tables created");
	}
	
	public void saveAttendy(ContentValues v)
	{	Log.i("Attendy-",v.toString());
		mdb = this.getWritableDatabase();
		String TB_NAME = "attendy";
		mdb.insert(TB_NAME, null, v);
		mdb.close();
		Log.i("Saved Attendy", "Saved Attendy");
		Attendy att = new Attendy();
		att.setAttending(v.getAsString("attending"));
		att.setConfServerId(v.getAsString("confServerId"));
		try {
			att.setCreatedDate(Utilities.sdfFull.parse(v.getAsString("createdDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		att.setEmailId(v.getAsString("emailId"));
		att.setConfId(Integer.parseInt(v.getAsString("confId")));
		PostAttendyToURL pttu = new PostAttendyToURL(att);
		pttu.execute();
		
	}
	
	public int fetchAttendy(String possibleEmail, String confServerId) {
		mdb = this.getReadableDatabase();

		String query = "SELECT * FROM attendy where emailId = '" + possibleEmail + "' AND confServerId = '" + confServerId + "'";
		// String query = "select * from attendy";
		Cursor curs = mdb.rawQuery(query, null);
		
		return curs.getCount();

	}
	public void setRatings(ContentValues v_feedback, int agendaId, Date createdDate, String agendaServerId)
	{
		mdb = this.getWritableDatabase();
		String TB_NAME = "feedback";
		mdb.insert(TB_NAME, null, v_feedback);
		mdb.close();
		
		Feedback feedback = new Feedback();
		feedback.setAgendaServerId(agendaServerId);
		feedback.setCreatedDate(createdDate);
		feedback.setFeedback(v_feedback.getAsString("feedback"));
		feedback.setRating(Float.parseFloat(v_feedback.getAsString("rating")));
		feedback.setEmailId(v_feedback.getAsString("emailId"));
		
		PostJsonToURL postjs = new PostJsonToURL(feedback);
		postjs.execute("");
				
	}
	
	public void updateLikeFromConference(String confId, String like) {
		mdb = this.getWritableDatabase();
		Log.i("Conf ID", "" + confId + like);
		ContentValues v = new ContentValues();
		v.put("like", like);
		mdb.update("conference", v, "serverId='" + confId + "'", null);
		mdb.close();

	}
	public void add_data() 
	{	
		
		mdb = this.getWritableDatabase();

		ContentValues v_conf = new ContentValues();
		// adding data in conference table
		String TB_NAME = "conference";

		// v_conf.put("id", "1");
		v_conf.put("short_code", "ef345");
		v_conf.put("title", "Big Data Mangt THISisGOING ToBEaBig ConeferenceTitle Let's see how it appears");
		v_conf.put("description", "Techniques to manage large amount of data and paper presentation. "
				+ "Techniques to manage large amount of data and paper presentation. Techniques to "
				+ "manage large amount of data and paper presentation. Techniques to manage large "
				+ "amount of data and paper presentation.");
		v_conf.put("logo", "http://upload.wikimedia.org/wikipedia/en/d/d8/PNG_logo_small.png");
		v_conf.put("venue", "32, saraswati Complex, Near Petrol pump, CG ROAD, Ahmedabad");
		v_conf.put("longitude", "23.190863"); 
		v_conf.put("latitude", "-6.152344"); 
		v_conf.put("creator", "yojna1992@gmail.com");
		v_conf.put("email", "yojna1992@gmail.com");
		v_conf.put("website", "http://www.yahoo.com");
		v_conf.put("phone_no", "9228282439"); // contact person of conference.
		v_conf.put("start_date", "2014-07-12 09:00:00");
		v_conf.put("end_date", "2014-07-12 15:00:00");
		v_conf.put("is_agenda_final", "true");
		mdb.insert(TB_NAME, null, v_conf);
		Log.i("Successfull insertion 1", "Successfull insertion");
		v_conf.clear();

		// v_conf.put("id", "2");
		v_conf.put("short_code", "ef345");
		v_conf.put("title", "Robotics");
		v_conf.put("description", "New Research work performed in ROBOTICS.");
		v_conf.put("logo",
				"http://duraspace.org/sites/all/themes/dura/space/logo.png");
		v_conf.put("venue", "CG ROAD, Ahmedabad");
		v_conf.put("longitude", "23.039568");
		v_conf.put("latitude", "72.566004");
		v_conf.put("creator", "yojna1992@gmail.com");
		v_conf.put("email", "ankitjainist@gmail.com");
		v_conf.put("website", "http://www.amazon.com");
		v_conf.put("phone_no", "7799829955"); // contact person of conference.
		v_conf.put("start_date", "2014-09-13 10:00:00");
		v_conf.put("end_date", "2014-09-15 14:30:00");
		v_conf.put("is_agenda_final", "true");
		mdb.insert(TB_NAME, null, v_conf);
		Log.i("Successfull insertion 2 ", "Successfull insertion");
		v_conf.clear();

		// v_conf.put("id", "3");
		v_conf.put("short_code", "HealthHygine");
		v_conf.put("title", "Health and Hygiene");
		v_conf.put("description",
				"Tips for Healthy Health and long life. How to manage weight.");
		v_conf.put("logo",
				"http://www.microlinks.org/sites/microlinks/files/news/images/AC%20Conference%20logo_0.jpg");
		v_conf.put("venue",
				"BK School of Management, Gujarat university, Ahmedabad");
		v_conf.put("longitude", "21.903353");
		v_conf.put("latitude", "73.035049");
		v_conf.put("creator", "yojna1992@gmail.com");
		v_conf.put("email", "chandasheikh90@gmail.com");
		v_conf.put("website", "http://www.google.co.in");
		v_conf.put("phone_no", "7799829955"); // contact person of conference.
		v_conf.put("start_date", "2014-03-05 09:15:00");
		v_conf.put("end_date", "2014-03-06 17:00:00");
		v_conf.put("is_agenda_final", "true");
		mdb.insert(TB_NAME, null, v_conf);
		Log.i("Successfull insertion 3 ", "Successfull insertion");
		v_conf.clear();

		TB_NAME = "agenda";
		// Adding data in agenda table
		//v_conf.put("id","101"); // id of agenda 
		v_conf.put("conf_id","1"); // foreign key from conference table 
		v_conf.put("title","Data Collection techniques"); // title of the talk
		v_conf.put("abstract","Today's world og big data questions arise that how to manage data? But before management collection of data is needed. "
				+ "How to find sources for collection is a challenging. I am gonaa show from where and how to collect data. "); // abstract of talk 
		v_conf.put("venue","F12, first floor, Saraswati Complex"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-07-12 09:30:00"); // date and time of talk
		v_conf.put("duration",3600); // duration of talk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
	//	v_conf.put("id","102"); // id of agenda 
		v_conf.put("conf_id","1"); // foreign key from conference table 
		v_conf.put("title","shortest path algos"); // title of the talk
		v_conf.put("abstract","I am gonna introduce a new algorithm to find the shortst path."); // abstract of talk 
		v_conf.put("venue","F12, first floor, Saraswati Complex"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-05-12 10:30:00"); // date and time of talk
		v_conf.put("duration",5400); // duration of talk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
	//	v_conf.put("id","103"); // id of agenda 
		v_conf.put("conf_id","1"); // foreign key from conference table 
		v_conf.put("title","Automation of Use Case Diagram"); // title of the talk
		v_conf.put("abstract","Here s/w will gonna check for the correctness of the use-case."); // abstract of talk 
		v_conf.put("venue","F12, first floor, Saraswati Complex"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-07-12 13:00:00"); // date and time of talk
		v_conf.put("duration",3660); // duration of talk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
	//	v_conf.put("id","104"); // id of agenda 
		v_conf.put("conf_id","1"); // foreign key from conference table 
		v_conf.put("title","Analysis of Data"); // title of the talk
		v_conf.put("abstract","A new technique of analysis where we don't have to store large mount of data."); // abstract of talk 
		v_conf.put("venue","F12, first floor, Saraswati Complex"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-07-12 14:00:00"); // date and time of talk
		v_conf.put("duration",3600); // duration of talk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		
//		v_conf.put("id","201"); // id of agenda 
		v_conf.put("conf_id","2"); // foreign key from conference table 
		v_conf.put("title","War between robots. "); // title of the talk
		v_conf.put("abstract","Some tips while creating robots for fighting. Importance of robots in our life."); // abstract of talk 
		v_conf.put("venue","Activity HAll, BK School of Management"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-09-13 10:00:00"); // date and time of talk
		v_conf.put("duration",18000); // duration of tlk in seconds  end: 15-3=2014, 14:30
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
//		v_conf.put("id","202"); // id of agenda 
		v_conf.put("conf_id","2"); // foreign key from conference table 
		v_conf.put("title","Manufacturing robots"); // title of the talk
		v_conf.put("abstract","Robot is manufacturing a robot."); // abstract of talk 
		v_conf.put("venue","Activity HAll, BK School of Management"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-09-13 15:15:00"); // date and time of talk
		v_conf.put("duration",6300); // duration of tlk in seconds  end: 15-3=2014, 14:30
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
	//	v_conf.put("id","203"); // id of agenda 
		v_conf.put("conf_id","2"); // foreign key from conference table 
		v_conf.put("title","Artificial hand feels what you touch"); // title of the talk
		v_conf.put("abstract","It's not quite the bionics of science fiction, but European researchers have created a robotic hand that gave an amputee a sense of touch he hadn't felt in a decade."); // abstract of talk 
		v_conf.put("venue","Activity HAll, BK School of Management"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-09-14 10:00:00"); // date and time of talk
		v_conf.put("duration",25200); // duration of tlk in seconds  end: 15-3=2014, 14:30
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
//		v_conf.put("id","204"); // id of agenda 
		v_conf.put("conf_id","2"); // foreign key from conference table 
		v_conf.put("title","E-puck mobile robot"); // title of the talk
		v_conf.put("abstract","The e-puck is a small (7 cm) differential wheeled mobile robot. It was originally designed for micro-engineering education by Michael Bonani and Francesco Mondada at the ASL laboratory of Prof. Roland Siegwart at EPFL (Lausanne, Switzerland). The e-puck is open hardware and its onboard software is open source, and is built and sold by several companies."); // abstract of talk 
		v_conf.put("venue","Activity HAll, BK School of Management"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-09-15 10:00:00"); // date and time of talk
		v_conf.put("duration",12600); // duration of tlk in seconds  end: 15-3=2014, 14:30
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
//		v_conf.put("id","301"); // id of agenda 
		v_conf.put("conf_id","3"); // foreign key from conference table 
		v_conf.put("title","Bad Breath"); // title of the talk
		v_conf.put("abstract","This is something that many people worry about and feel conscious of at times. There are several things that can cause bad breath, eg diseases of the teeth, gums and mouth, indigestion and some other health problems."); // abstract of talk 
		v_conf.put("venue","34, shell peel, CG ROAD"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-03-05 09:15:00"); // date and time of talk
		v_conf.put("duration",9900); // duration of tlk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
	//	v_conf.put("id","302"); // id of agenda 
		v_conf.put("conf_id","3"); // foreign key from conference table 
		v_conf.put("title","Food poisoning"); // title of the talk
		v_conf.put("abstract","Food poisoning is an illness that you can develop after eating food that is contaminated with harmful bacteria. You can feel sick, vomit, have abdominal pains and diarrhoea a couple of hours to a day or so after eating the contaminated food. You can't always tell if food has been contaminated because the bacteria often don't make the food smell or taste different. Following are some ideas on how you can prevent those bacteria getting into your food at home (or work)."); // abstract of talk 
		v_conf.put("venue","34, shell peel, CG ROAD"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-03-05 13:15:00"); // date and time of talk
		v_conf.put("duration",13500); // duration of tlk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
//		v_conf.put("id","303"); // id of agenda 
		v_conf.put("conf_id","3"); // foreign key from conference table 
		v_conf.put("title","Personal hygiene - taking care of your body"); // title of the talk
		v_conf.put("abstract","Caring about the way you look is important to your self esteem (what you think about yourself).  This topic gives you some ideas on looking your best.  By the way, you don't need to wear the latest designer clothing to look good. "); // abstract of talk 
		v_conf.put("venue","34, shell peel, CG ROAD"); // room no, in a building of particular talk
		v_conf.put("start_time","2014-03-06 10:00:00"); // date and time of talk
		v_conf.put("duration",25200); // duration of tlk in seconds 
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		
		
		v_conf.clear();
		// adding data to speaker table
		TB_NAME="people";
		//v_conf.put("_id","1101");
		v_conf.put("agenda_id","1"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","shailesh yadav"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/pratik-thanawala.jpg"); //photo of speaker
		v_conf.put("tag_line","JUST JOKING");
		v_conf.put("bio","Working at Infosoft, pune"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		//v_conf.put("_id","1102");
		v_conf.put("agenda_id","1"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","Neha Chauhan"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/kunjal-gajjar.jpg"); //photo of speaker
		v_conf.put("tag_line","Do your Best");
		v_conf.put("bio","Teaching at IIT, Rurki"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		//v_conf.put("_id","1103");
		v_conf.put("agenda_id","1"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","Bipin Mehta"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/bipin-mehta.jpg"); //photo of speaker
		v_conf.put("tag_line","Paisa hai to fir kya tention");
		v_conf.put("bio","CEO of Gold Fish, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		//v_conf.put("_id","1104");
		v_conf.put("agenda_id","1"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","Bill Gates"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://classicalhomeschooling.com/wp-content/uploads/2010/01/gates.jpg"); //photo of speaker
		v_conf.put("tag_line","I am richest.");
		v_conf.put("bio","CEO of Microsoft, US"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
	//	Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		//v_conf.put("_id","1102");
		v_conf.put("agenda_id","2"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","Minakshi Dave"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/kunjal-gajjar.jpg"); //photo of speaker
		v_conf.put("tag_line","Do your Best");
		v_conf.put("bio","Teaching at IIT, Kanpur"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		//Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
	
		//v_conf.put("_id","1102");
		v_conf.put("agenda_id","3"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","Jagrat Jain"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/kunjal-gajjar.jpg"); //photo of speaker
		v_conf.put("tag_line","Judge it");
		v_conf.put("bio","Adani, Dahej"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
				
		v_conf.put("agenda_id","4"); // foreign ky from agenda table
		v_conf.put("conf_id","1"); // foreign key from conference table
		v_conf.put("name","Sulochana Jain"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://hindi-movies-songs.com/actor-photos/dzlitem217.jpg"); //photo of speaker
		v_conf.put("tag_line","Judge it");
		v_conf.put("bio","Working at NGo"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();


		//v_conf.put("_id","1105");
		v_conf.put("agenda_id","5"); // foreign ky from agenda table
		v_conf.put("conf_id","2"); // foreign key from conference table
		v_conf.put("name","kuntal Patel"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/kuntal-patel.jpg"); //photo of speaker
		v_conf.put("tag_line","Provide Safety to your packet");
		v_conf.put("bio","Prof. at AESICS, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		
		//v_conf.put("_id","1106");
		v_conf.put("agenda_id","6"); // foreign ky from agenda table
		v_conf.put("conf_id","2"); // foreign key from conference table
		v_conf.put("name","Jaydeep Rathod"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/jaideepsinh-raulji.jpg"); //photo of speaker
		v_conf.put("tag_line","Perform your best.");
		v_conf.put("bio","Prof. at LD Engg, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		//v_conf.put("_id","1107");
		v_conf.put("agenda_id","7"); // foreign ky from agenda table
		v_conf.put("conf_id","2"); // foreign key from conference table
		v_conf.put("name","Siddhi Shah"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/siddhi-shah.jpg"); //photo of speaker
		v_conf.put("tag_line","Knowledge is power");
		v_conf.put("bio","Prof. at LD Engg, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		//v_conf.put("_id","1108");
		v_conf.put("agenda_id","8"); // foreign ky from agenda table
		v_conf.put("conf_id","2"); // foreign key from conference table
		v_conf.put("name","Heena Timani"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.ahduni.edu.in/images/faculty/heena-timani.jpg"); //photo of speaker
		v_conf.put("tag_line","Sab Thik Thak h.");
		v_conf.put("bio","Prof. at LD Engg, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		
		
		//v_conf.put("_id","1109");
		v_conf.put("agenda_id","9"); // foreign ky from agenda table
		v_conf.put("conf_id","3"); // foreign key from conference table
		v_conf.put("name","Chanda Sheikh"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://www.name-list.net/img/images.php/Chanda_1.jpg"); //photo of speaker
		v_conf.put("tag_line","Life is our GOAL.");
		v_conf.put("bio","Prof. at KK Shastri College, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
		
		//v_conf.put("_id","1110");
		v_conf.put("agenda_id","10"); // foreign ky from agenda table
		v_conf.put("conf_id","3"); // foreign key from conference table
		v_conf.put("name","Ankit Jain"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","https://www.crowdchat.net/about/_include/img/profile/ankit-jain.jpg"); //photo of speaker
		v_conf.put("tag_line","Ye Meri Life hai");
		v_conf.put("bio","Prof. at IIT, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();
		
	//	v_conf.put("_id","1111");
		v_conf.put("agenda_id","11"); // foreign ky from agenda table
		v_conf.put("conf_id","3"); // foreign key from conference table
		v_conf.put("name","Shirin Ansari"); // name of speaker 
		v_conf.put("type","SPEAKER"); //
		v_conf.put("photo","http://cdn.sheknows.com/articles/2010/12/young-business-woman.jpg"); //photo of speaker
		v_conf.put("tag_line","Always Rock");
		v_conf.put("bio","Working at Nascom, Ahmedabad"); //education and post of speaker
		mdb.insert(TB_NAME,null,v_conf);
		Log.i("Successfull insertion 2 ","Successfull insertion");
		v_conf.clear();	
		
		mdb.close();
		addBarCampConf();
	}
	
	private Conference getConferenceDetail(Cursor curs)
	{
		Conference conf=new Conference();
			
		{	
		conf.setId(curs.getInt(0));
		conf.setShortCode(curs.getString(1));
		conf.setTitle(curs.getString(2));
		conf.setDescription(curs.getString(3));
		conf.setLogo(curs.getString(4));
		conf.setVenue(curs.getString(5));
		conf.setLongitude(curs.getString(6));
		conf.setLatitude(curs.getString(7));
		conf.setCreator(curs.getString(8));
		conf.setWebsite(curs.getString(9));
		conf.setEmail(curs.getString(10));
		conf.setPhoneNo(curs.getString(11));
		conf.setServerId(curs.getString(15));
		conf.setLike(curs.getString(16));		
		Log.i("Like", conf.getLike());
		Date dt = null;
		try {
				dt = (Date) Utilities.sdfFull.parse(curs.getString(12));
				conf.setStartDate(dt);
		} catch (ParseException e) {
			conf.setStartDate(null);
			Log.e("ConferenceListAdapter", "Date parse exception", e);
		}
	
		
		try {
			dt = (Date) Utilities.sdfFull.parse(curs.getString(13));
			
			conf.setEndDate(dt);
		} catch (ParseException e) {
			conf.setEndDate(null);
			Log.e("ConferenceListAdapter", "Date parse exception", e);
		}
			
		conf.setIsAgendaFinal(curs.getString(14));
		// TO get detail of agenda
		conf.setAgendas(displayAgendaRecord(conf.getId()));
		
		}
		
		return conf;
	}
	
	private Agenda getAgendaDetail(Cursor curs)
	{
		
		
		Agenda agd=new Agenda();
		
		{  
			
		agd.setId(curs.getInt(0));
		agd.setConfId(curs.getInt(1));
		agd.setTitle(curs.getString(2));
		agd.setAbstractOfTalk(curs.getString(3));
		agd.setVenue(curs.getString(4));
		agd.setServerId(curs.getString(7));
				
		Date dt = null;
		try {
			
		
		dt = (Date) Utilities.sdfFull.parse(curs.getString(5));
		
			agd.setStartTime(dt);
		} catch (ParseException e) {
			agd.setStartTime(null);
			Log.e("ConferenceListAdapter", "Date parse exception", e);
		}
		agd.setDuration(curs.getLong(6));
		agd.setPeople(displaySpeakersOfAgenda(agd.getConfId(), agd.getId()));
		
		}
		
		return agd;
	}


	private People getPeopleDetail(Cursor curs)
	{
			
		People pol=new People();
		{
		pol.setId(curs.getInt(0));
		pol.setConfId(curs.getInt(1));
		pol.setAgendaId(curs.getInt(2));
		pol.setSpeakerName(curs.getString(7));
		pol.setType(curs.getString(3));
		pol.setPhoto(curs.getString(4));
		pol.setTagLine(curs.getString(5));
		pol.setBio(curs.getString(6));
		pol.setServerId(curs.getString(8));
		}
		
		
		return pol;
	}
	 private Feedback getFeedbackDetail(Cursor curs)
	 {
		curs.moveToLast(); 
		Feedback feedback = new Feedback();
		{	Log.i("Cursor :", ""+curs);
			feedback.setId(curs.getInt(0));
			feedback.setUserName(curs.getString(1));
			feedback.setEmailId(curs.getString(2));
			
		//	Log.i("email id",curs.getString(2));
			
			feedback.setFeedback(curs.getString(3));
			feedback.setAgendaId(curs.getInt(5));
			feedback.setRating(curs.getFloat(7));
			feedback.setAgendaServerId(curs.getString(6));
			Log.i("DA Feedback fun :",""+feedback);
			Date dt = null;
			try {
				dt = (Date) Utilities.sdfFull.parse(curs.getString(4));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				Log.i("Error","Parsing error");
				e.printStackTrace();
			}
			feedback.setCreatedDate(dt);
		} 
		return feedback;
		 
	 }
	 public Attendy getAttendy(Cursor curs)
	 {
		 curs.moveToFirst();
		 Attendy att = new Attendy();
		 att.setId(curs.getInt(0));
		 att.setConfId(curs.getInt(2));
		 att.setConfServerId(curs.getString(1));
		 att.setEmailId(curs.getString(3));
		 att.setAttending(curs.getString(4));
		 try {
			att.setCreatedDate(Utilities.sdfFull.parse(curs.getString(5)));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return att;		 
	 }
	public Conference getConfRecord(int id)
	{
		
		Cursor curs;
		mdb = this.getReadableDatabase();
		String query = "select * from conference where _id = " +id;
		
		curs = mdb.rawQuery(query,null);
		curs.moveToNext();
		Conference conf = getConferenceDetail(curs);
		return conf;
	}
	public List<Conference> getAllConferences()
	{
		
		Cursor curs;
		mdb = this.getReadableDatabase();
		String selectquery = "select * from conference ORDER BY start_date DESC";
		curs = mdb.rawQuery(selectquery, null);
		
		 List<Conference> conference  = new ArrayList<Conference>();
		 Conference cf;
		
		 while(curs.moveToNext())
		 {
			 cf = getConferenceDetail(curs); 
			 conference.add(cf);
		 }
		 return conference;		
	}
	
	
	public List<Conference> searchConference(String s)
	{		
		Cursor curs;
		mdb = this.getReadableDatabase();
		 String selectquery = "SELECT * FROM conference where short_code ='" + s + "'";
		curs = mdb.rawQuery(selectquery, null);	
		Conference conf = new Conference();
		List<Conference> ltConf = new ArrayList<Conference>();

		if(curs!=null && curs.getCount()>0)
		{
			if(curs.getCount()>1 )
			{
					while(curs.moveToNext())
					{
						conf = getConferenceDetail(curs);
						ltConf.add(conf);
					}
			}
			else
			{
				curs.moveToNext();
				conf= getConferenceDetail(curs);
				ltConf.add(conf);
			
			}
		}
		else
		{
			conf.setTitle("No records Found");
			conf.setLogo(null);
			conf.setShortCode(null);
			conf.setDescription(null);
			conf.setStartDate(Calendar.getInstance().getTime());
			ltConf.add(conf);
		}
			return ltConf;
	}
	

	
	 public ArrayList<Agenda> displayAgendaRecord(int id) {
		 
		 Cursor curs;
			mdb = this.getReadableDatabase();
			
			 String selectquery = "SELECT *  FROM agenda  where conf_id = " + id +" ORDER By start_time ASC";
			curs = mdb.rawQuery(selectquery, null);
			ArrayList<Agenda> agenda = new ArrayList<Agenda>();
			Agenda agd;
			
			while(curs.moveToNext())
			{
				agd=getAgendaDetail(curs);
				agenda.add(agd);
			

			}
			
		
		    return agenda;
	    }

	public List<People> displaySpeakersOfAgenda(int id, int agd_id)
	{
		Cursor curs;
		mdb=this.getReadableDatabase();
		String selectquery= " select * from people where conf_id = " +id + " and agenda_id = " +agd_id;
		curs=mdb.rawQuery(selectquery, null);
		List<People> people = new ArrayList<People>();
		People pl;
		while(curs.moveToNext())
		{
				pl =getPeopleDetail(curs); 
				people.add(pl);
				
		}return people;
	}
	public List<People> speakersOfconf(int id)
	{
		
		Cursor curs;
		mdb=this.getReadableDatabase();
		String selectquery= " select * from people where conf_id = " +id;
		curs=mdb.rawQuery(selectquery, null);

		List<People> pl = new ArrayList<People>();
		People people; 
		while(curs.moveToNext())
		{
			people = getPeopleDetail(curs);
			pl.add(people);
			
		}
		
		return pl;
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
	
	private void addBarCampConf() {
		mdb = this.getWritableDatabase();
		ContentValues v_conf = new ContentValues();

		v_conf.put("_id", "5");
		v_conf.put("short_code", "barcampblr");
		v_conf.put("title", "Barcamp Bangalore");
		v_conf.put(
				"description",
				"Barcamp Bangalore is an open event focused around people, ideas and collaboration. "
						+ "There is no fixed format and agenda. If you have an interesting topic to share or want to "
						+ "collaborate with folks with a variety of experience, Barcamp is the place for you. \n\nSince "
						+ "its inception in Bangalore in 2006, BCB has been a place for awesome people with ideas in "
						+ "their heads to talk and share or just hang out with their peers and friends.You can expect "
						+ "sessions on variety of topics like");
		v_conf.put("logo", "http://enroller.in/conf-assist/barcamp.png");
		v_conf.put("venue", "SAP Labs, Whitefield, Bangalore");
		v_conf.put("longitude", "12.9794468");
		v_conf.put("latitude", "77.71559");
		v_conf.put("email", "");
		v_conf.put("website", "http://BarcampBangalore.org/bcb/");
		v_conf.put("phone_no", ""); // contact person of conference.
		v_conf.put("start_date", "2014-03-29 10:00:00");
		v_conf.put("end_date", "2014-03-29 14:30:00");
		v_conf.put("is_agenda_final", "false");
		mdb.insert("conference", null, v_conf);


		/********************
		 * Agenda 
		 *******************/
		v_conf.clear();
		v_conf.put("_id", "501");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Registration");
		v_conf.put("abstract", "");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 08:00:00");
		v_conf.put("duration", 3600);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "502");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Introduction");
		v_conf.put("abstract", "");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 09:00:00");
		v_conf.put("duration", 1800);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "503");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Big Data Basics");
		v_conf.put("abstract", "Big Data, Data Science & Analytics. Data Analytics Life cycle. Analytics technology & tools � Map Reduce, Hadoop. Methods to apply this knowledge to real work business challenges.");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 09:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "504");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Introduction to Browser Internals");
		v_conf.put("abstract", "You might have written lots of HTML, CSS and Javascript code. Have you ever wondered how it works together? This session focuses on the basics of"
				+ "\n\n    Internal DOM Structure"
				+ "\n    Layout Engine"
				+ "\n    JavaScript Engine"
				+ "\n    Various Developer Tools available in the browsers"
				+ "\n    How to access/change the DOM objects"
				+ "\n    and much more fun demos.");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 10:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "505");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Why you shouldn�t work at a startup");
		v_conf.put("abstract", "*Bait alert*"
				+ "\n\nSo, I work at a startup that champions startups and have worked at startups all through my professional life. While a lot of people will tell you that working at a startup is the next best thing to being an entrepreneur, you need to know what you�re signing up for."
				+ "\nThis is going to be a talk about everything that can go wrong in working for a startup and why you shouldn�t work at one.");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 11:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "506");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Lunch");
		v_conf.put("abstract", "");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 12:30:00");
		v_conf.put("duration", 3600);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "507");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Techlash");
		v_conf.put("abstract", "");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 13:30:00");
		v_conf.put("duration", 3600);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "508");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Introduction to security testing/Ethical Hacking");
		v_conf.put("abstract", "Meet an ethical hacker while you are here at the Bar Camp Bangalore and learn what qualifies for a Hack?\n"
				+ "\n    What goes on in the mind of an ethical hacker and a hacker?"
				+ "\n    Learn new and the trending technologies to be right there at the right moment."
				+ "\n    Been Victimized? Learn what you could have done differently."
				+ "\n    Learn more on security browser addon�s"
				+ "\n\nLETS HACK THE WORLD!!!!");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 14:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "509");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Why people Lie � A choice between being NICE and being REAL");
		v_conf.put("abstract", "Sounds like a simple question, yet we wonder why do people who appear nice in person and talk wisely become an entirely opposite person the moment they leave the room. Why can�t they be their true self while talking to you in the first place."
				+ "\n\nFind answers to similar questions. and also find out where to draw the line.");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 15:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "510");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Corruption control using technology");
		v_conf.put("abstract", "Avoid the corruption and remove the black money concept in India using this technology we can trace all the transaction of the every citizen of the India.");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 16:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", "511");
		v_conf.put("conf_id", "5");
		v_conf.put("title", "Feedback");
		v_conf.put("abstract", "");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 17:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		/*****************
		 * Speakers *
		 *****************/

		v_conf.clear();
		v_conf.put("_id","501");
		v_conf.put("agenda_id", "503");
		v_conf.put("conf_id", "5");
		v_conf.put("name", "Viji");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://profile.ak.fbcdn.net/hprofile-ak-ash2/t5/1115758_1255925592_1913785289_q.jpg");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id","502");
		v_conf.put("agenda_id", "504");
		v_conf.put("conf_id", "5");
		v_conf.put("name", "sivaa");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id","503");
		v_conf.put("agenda_id", "505");
		v_conf.put("conf_id", "5");
		v_conf.put("name", "Raghu");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://profile.ak.fbcdn.net/hprofile-ak-prn2/t5/1118076_100000288402951_1355397868_q.jpg");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id","504");
		v_conf.put("agenda_id", "508");
		v_conf.put("conf_id", "5");
		v_conf.put("name", "solidmonster");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://0.gravatar.com/avatar/bacfe76c85d8b3efc8ae66c4e505e858?size=400px");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "I'm what i'm.. Well.. I live by my own rules n hates restrictions of any kind. I believe that life is bout taking risks, making choices, changing little for others, enjoying the present, not worrying about future and taking the best from past ahead with you...Take it bit by bit and everything is possible if you want it of any kind! I'm fun loving, simple in thoughts, Love my family and buddies. Beware guys... I'm a very good observer!");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id","505");
		v_conf.put("agenda_id", "509");
		v_conf.put("conf_id", "5");
		v_conf.put("name", "Mahanthesh Shadakshari");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id","506");
		v_conf.put("agenda_id", "510");
		v_conf.put("conf_id", "5");
		v_conf.put("name", "Aditya Bhushan Dwivedi");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://profile.ak.fbcdn.net/hprofile-ak-prn2/t5.0-1/261062_1216302536_82314724_q.jpg");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		
		/******************
		 * Adding 2nd Track
		 ******************/
		v_conf.clear();
		v_conf.put("_id", 512);
		v_conf.put("conf_id", 5);
		v_conf.put("title", "Making Music with Free Software Tools");
		v_conf.put("abstract", "Ever wanted to create some cool music or tunes? Let�s do this! "
				+ "\n\nSoftware Instruments:  Audacity and Hydrogen� and some web apps and Chrome extensions. "
				+ "No expertise of  Sa Re Ga Ma Pa needed. "
				+ "\n\nWhat to Bring:  Laptop or Smartphone. "
				+ "Any tune you are/were/have been humming. "
				+ "\n\nLevel: Beginner");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 09:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", 513);
		v_conf.put("conf_id", 5);
		v_conf.put("title", "Little Known Secrets to convey anything & everything Visually !!");
		v_conf.put("abstract", "Whether you are an entrepreneur (or) a techie working for a company (or) a Manager handling a team (or) a student, its really important that you communicate effectively to your audience."
				+ "\n\nText is diminishing. The most successful ones � right from Marketing Guru Seth Godin, to Search Major Google, have reduced the usage of text and has been emphasizing more on Visuals, especially in the digital era."
				+ "\n\nBut, not all of us could convey stuff visually. So, Let me introduce you to some of the little known secrets of visual communication / visual story telling. With very little effort, you can invoke the artist in you."
				+ "\n\nWe will explore key elements of visual communication, loads of samples and funny illustrations. ");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 10:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", 514);
		v_conf.put("conf_id", 5);
		v_conf.put("title", "OpenStack in action � Powering IaaS Cloud");
		v_conf.put("abstract", "Recently OpenStack celebrated 3rd anniversary.Best known for driving cloud innovation and accelerating cloud implementation. "
				+ "OpenStack India user group is emerging as one of the biggest group across globe."
				+ "\n\nThis session is purely for beginner to understand basics of OpenStack."
				+ "\n- .History and Projects"
				+ "\n- How to contribute"
				+ "\n- Success stories"
				+ "\n\nJoin us and get involved !!");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 11:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", 515);
		v_conf.put("conf_id", 5);
		v_conf.put("title", "How to keep Android open source");
		v_conf.put("abstract", "We keep hearing that Android is not really open source. The code of it available for all after its released. "
				+ "\n\nBut there is a catch now. Google Play services. Google is moving a lot of its code, which could have been part of Android OS to Play services. Things like Location API�s, GCM, Games etc. "
				+ " This is just the beginning, there are more things coming in. Lets discuss the pros and cons of this and also try and think about ways in which we can keep things from going down south.");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 14:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", 516);
		v_conf.put("conf_id", 5);
		v_conf.put("title", "Stupidity on the Internet");
		v_conf.put("abstract", "We talk about stupid behaviour on the Internet");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 15:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);

		v_conf.clear();
		v_conf.put("_id", 517);
		v_conf.put("conf_id", 5);
		v_conf.put("title", "Lets experience Python !");
		v_conf.put("abstract", "Python has been gaining popularity over other scripting languages. "
				+ "In this session I will try to make you guys a bit familiar and more comfortable to this �battery-charged� programming language."
				+ "\n\nWhat can you expect from this session :"
				+ "\n-Introduction"
				+ "\n-Basics"
				+ "\n-Few useful modules"
				+ "\n-Its Application"
				+ "\n\nNote :: To be good in python, you should just be knowing what is variable and constants :D"
				+ "So �Lets experience Python� :D"
				+ "\n\nYou can reach me via twitter @adilimroz");
		v_conf.put("venue", "");
		v_conf.put("start_time", "2014-03-29 16:30:00");
		v_conf.put("duration", 2700);
		mdb.insert("agenda", null, v_conf);
		
		/*****************
		 * Speakers - 2nd Track*
		 *****************/

		v_conf.clear();
		v_conf.put("_id",507);
		v_conf.put("agenda_id", 512);
		v_conf.put("conf_id", 5);
		v_conf.put("name", "Vivekk");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://2.gravatar.com/avatar/4c776f36af429e9382ea60c3e43d6cb6?size=400px");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id",508);
		v_conf.put("agenda_id", 513);
		v_conf.put("conf_id", "5");
		v_conf.put("name", "Ramesh Kumar");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://0.gravatar.com/avatar/c924a8e4258efbc7d88797bd26444e1a?s=96&d=wavatar&r=G");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id",509);
		v_conf.put("agenda_id", 514);
		v_conf.put("conf_id", 5);
		v_conf.put("name", "Sajid Akhtar");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://profile.ak.fbcdn.net/hprofile-ak-ash1/t5.0-1/187232_687812219_1047652557_q.jpg");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id",510);
		v_conf.put("agenda_id", 515);
		v_conf.put("conf_id", 5);
		v_conf.put("name", "the100rabh");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://1.gravatar.com/avatar/bb6caa13742a331aad8b493034663a64?s=256");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id",511);
		v_conf.put("agenda_id", 516);
		v_conf.put("conf_id", 5);
		v_conf.put("name", "Sathya Bhat");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://profile.ak.fbcdn.net/hprofile-ak-ash1/t5.0-1/372715_543508896_1670304571_q.jpg");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);

		v_conf.clear();
		v_conf.put("_id",512);
		v_conf.put("agenda_id", 517);
		v_conf.put("conf_id", 5);
		v_conf.put("name", "Adil Imroz");
		v_conf.put("type", "SPEAKER"); 
		v_conf.put("photo","http://profile.ak.fbcdn.net/hprofile-ak-prn2/t5/186597_100000249718194_1572897998_q.jpg");
		v_conf.put("tag_line", "");
		v_conf.put("bio", "");
		mdb.insert("people", null, v_conf);
	
	
		mdb.close();
	}
}