package in.enroller.confassist;
//#8
import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.Conference;

import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TalkListActivity extends Activity implements OnItemClickListener {

	ImageView iv;
	TextView confName,confCode, title;
	ListView lv;
	
	DatabaseAdapter da;
	Conference conf;
	TalkListAdapter adapt;	
	String id;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_talk_list);
		ActionBar actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
	
		iv=(ImageView)findViewById(R.id.imageView1);
	
		confName=(TextView)findViewById(R.id.textView1);
		confCode=(TextView)findViewById(R.id.textView2);
		lv=(ListView)findViewById(R.id.listView1);
		
		Bundle b = this.getIntent().getExtras();
	    id = b.getString("ID");
		int i = Integer.parseInt(id);		
		
	    da = new DatabaseAdapter(this);
	    conf = da.getConfRecord(i);
	    confName.setText(conf.getTitle());
	    confCode.setText("#" +conf.getShortCode());
	    HomeActivity.imageLoader.DisplayImage(conf.getLogo(), iv);
	    this.setTitle(conf.getTitle());
	    final Conference conf = da.getConfRecord(i);
	    List<Agenda> agd = da.displayAgendaRecord(i);
	    adapt = new TalkListAdapter(this, agd);
	   	lv.setAdapter(adapt);
	   	lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id1) {
				Intent i = new Intent(TalkListActivity.this,TalkConfActivity.class);
				Agenda ag=(Agenda)view.getTag();
				i.putExtra("conferenceID", ""+conf.getId());
				i.putExtra("agendaID", "" + ag.getId());
				startActivity(i);

			}
		});
	   	lv.setSelection(0);
		
	}	public void onClick(DialogInterface arg0, int arg1) {
		// TODO Auto-generated method stub
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.talk_list, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.action_settings:
			Toast.makeText(this, "setting selected", Toast.LENGTH_SHORT).show();

			Intent iset = new Intent(TalkListActivity.this, SettingScreenActivity.class);
			startActivity(iset);

			break;
		// action with ID action_settings was selected
		case R.id.action_about:
			Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(TalkListActivity.this, AboutScreenActivity.class);
			startActivity(i);
			break;
		case android.R.id.home:
			Intent ia = new Intent(TalkListActivity.this, HomeActivity.class);
			startActivity(ia);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}