package in.enroller.confassist.common;

import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Patterns;

public class Utilities {
	
	public static SimpleDateFormat sdfFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
	public static SimpleDateFormat sdfPrintFormat = new SimpleDateFormat("MMM dd, yyyy");
	public static SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");

	static{
		sdfFull.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	public static String findEmail(Context context)
	{
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(context).getAccounts();
		String possibleEmail = null;
		for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		    	possibleEmail = account.name;
		    	break;
		    }
		    
		}
		return possibleEmail;
	}
	public Boolean checkInternetConnectivity(Context context)
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
		if(info != null)
		{
			for(int i=0; i<info.length; i++)
			{
				
				if(info[i].getState() == NetworkInfo.State.CONNECTED)
				{
					return true;
					
				}
			}
			
		}
		return false;
		
	}
}
