package in.enroller.confassist;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.model.Conference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;





import android.R.color;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ConferenceListAdapter extends ArrayAdapter<Conference> {

	static SimpleDateFormat fmt ;
	public ConferenceListAdapter(Context context, List<Conference> conf) {
		super(context, R.layout.custom_list,conf); 
	}
	


	@Override
	public View getView(int position, View v, ViewGroup parent) {
		
		Conference conf = (Conference) getItem(position);
		
		if(v == null)
			v = LayoutInflater.from(getContext()).inflate(R.layout.custom_list, null);
		
		ImageView conf_logo;
	
		conf_logo = (ImageView) v.findViewById(R.id.imageViewCustomList);
		if(conf.getLogo()!=null || conf.getLogo()!=" ")
		{	conf_logo.setTag(conf.getLogo());
				HomeActivity.imageLoader.DisplayImage(conf.getLogo(), conf_logo);
		
		}
		else
		{	
				conf_logo.setImageResource(R.drawable.default_logo);
			
		}
		TextView conf_title = (TextView) v.findViewById(R.id.textView_confName);
		conf_title.setText(conf.getTitle());

		TextView shortCode = (TextView) v.findViewById(R.id.textView_shortCode);
		shortCode.setText("#" + conf.getShortCode());

		TextView desc, conf_date;
		desc = (TextView) v.findViewById(R.id.textView_description);
		desc.setText(conf.getDescription());

		conf_date = (TextView) v.findViewById(R.id.textView_date);

		String formattedDate = Utilities.sdfPrintFormat.format(conf.getStartDate());
		conf_date.setText(formattedDate);


		v.setTag(conf);

		//
		Calendar c = Calendar.getInstance();		

		SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy");
		String todayDate = df.format(c.getTime());
		System.out.println("Current time => " + todayDate +"========"+ formattedDate);

		  try {
			if(df.parse(todayDate).after(df.parse(formattedDate)))
				{
				  v.setBackgroundResource(R.color.back_1);
				  System.out.println(conf.getTitle() +" "+todayDate.compareTo(formattedDate));
				}
			 else {
				v.setBackgroundResource(color.white);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v;
	}
}
