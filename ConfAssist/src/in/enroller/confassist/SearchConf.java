package in.enroller.confassist;

import in.enroller.confassist.jsonserialization.ReadJsonFromUrl;
import in.enroller.confassist.model.Conference;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchConf extends Activity {

	Button bsearch;
	TextView t1;
	EditText ecode;
	ListView ldisplay;
	DatabaseAdapter db;
	ConferenceListAdapter custadpt;
	Boolean getConf=false;
	private ReadJsonFromUrl readJsonFromUrl;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_search_conf);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
	
		bsearch=(Button)findViewById(R.id.button1);
		ecode=(EditText)findViewById(R.id.editText1);
		
		ldisplay=(ListView)findViewById(R.id.listView1);
		db = new DatabaseAdapter(this);
					
		bsearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			
				String s;
				s=ecode.getText().toString();
				
				if(s.matches(""))
				{
					Toast.makeText(getApplicationContext(), "Enter Short Code to search", Toast.LENGTH_LONG).show();
				}
				else
				{
				
				List<Conference> conferences = db.getAllConferences();	
				for(int i=0;i<conferences.size();i++)
				{
					Conference conf = conferences.get(i);
					if(conf.getShortCode().equalsIgnoreCase(s))
					{
						getConf = true;
						
					}
					
				}
				if(!getConf){
				readJsonFromUrl = new ReadJsonFromUrl(getApplicationContext(),s);
				readJsonFromUrl.execute(s);
				}
				
				List<Conference> conf =  (List<Conference>) db.searchConference(s);
				
				custadpt=new ConferenceListAdapter(getApplicationContext(),conf);					
				refreshList();
				
				ldisplay.setAdapter(custadpt);				
				
				ldisplay.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						Intent i = new Intent(SearchConf.this,
								ConferenceActivity.class);
						Conference co = (Conference)view.getTag();
						i.putExtra("ID", "" + co.getId());
						startActivity(i);
					}
				});
			  }
			}
		});
		ldisplay.setSelection(0);
	}
	public void refreshList()
	{
		
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					custadpt.notifyDataSetChanged();
				}
			});
	}
}