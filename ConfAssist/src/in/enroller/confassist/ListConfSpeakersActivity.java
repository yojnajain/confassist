package in.enroller.confassist;
//Issue 11, screen #6 It shows the list of speakers of particular talk

import in.enroller.confassist.model.Conference;
import in.enroller.confassist.model.People;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListConfSpeakersActivity extends Activity {
	TextView confName, shortCode;
	ImageView confLogo;
	
	ListView speakerList;
	DatabaseAdapter db;
	int id;
	Conference conf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_list_conf_speakers);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		
		db = new DatabaseAdapter(this);
		Bundle b = this.getIntent().getExtras();
		id = Integer.parseInt( b.getString("ID"));
		
		confName = (TextView)findViewById(R.id.textView_ConfName);
		shortCode = (TextView)findViewById(R.id.textView_code);
		confLogo = (ImageView)findViewById(R.id.imageView_confLogo);
		speakerList = (ListView)findViewById(R.id.listView_speakerList);
		conf= new Conference();
		 conf = db.getConfRecord(id);
		
	
		confName.setText(conf.getTitle());
		shortCode.setText("#"+conf.getShortCode());
		this.setTitle(conf.getTitle());
		  HomeActivity.imageLoader.DisplayImage(conf.getLogo(), confLogo);
		  
		  List<People> pl =  db.speakersOfconf(id);
	    SpeakerListAdapter ad = new SpeakerListAdapter(this, pl);
		
	    speakerList.setAdapter(ad);
		
		speakerList.setSelection(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_conf_speakers, menu);
		return true;
	}

}
