package in.enroller.confassist.model;

import java.util.Date;

public class Feedback {

	private int id;
	private String userName;
	private String emailId;
	private String feedback;
	private Float rating;
	private Date createdDate;
	private int agendaId;
	private String agendaServerId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public Float getRating() {
		return rating;
	}
	public void setRating(Float rating) {
		this.rating = rating;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public int getAgendaId() {
		return agendaId;
	}
	public void setAgendaId(int agendaId) {
		this.agendaId = agendaId;
	}
	public String getAgendaServerId() {
		return agendaServerId;
	}
	public void setAgendaServerId(String agendaServerId) {
		this.agendaServerId = agendaServerId;
	}
	
}
