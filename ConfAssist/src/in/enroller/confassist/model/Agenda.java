package in.enroller.confassist.model;

import java.util.Date;
import java.util.List;


public class Agenda {

	
		private int id;
		private int confId;
		private String title;
		private String abstractOfTalk;
		private String venue;
		private Date startTime;
		private long duration;
		private String serverId;
		
		private List<People> people;
		
		public int getId() {
			return id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		
		public String getVenue() {
			return venue;
		}
		public void setVenue(String venue) {
			this.venue = venue;
		}
		
		public int getConfId() {
			return confId;
		}
		public void setConfId(int confId) {
			this.confId = confId;
		}
		public String getAbstractOfTalk() {
			return abstractOfTalk;
		}
		public void setAbstractOfTalk(String abstractOfTalk) {
			this.abstractOfTalk = abstractOfTalk;
		}
		public Date getStartTime() {
			return startTime;
		}
		public void setStartTime(Date startTime) {
			this.startTime = startTime;
		}
		public long getDuration() {
			return duration;
		}
		public void setDuration(long duration) {
			this.duration = duration;
		}
		public List<People> getPeople() {
			return people;
		}
		public void setPeople(List<People> people) {
			this.people = people;
		}
		public String getServerId() {
			return serverId;
		}
		public void setServerId(String serverId) {
			this.serverId = serverId;
		} 
		
}
