package in.enroller.confassist.model;

import java.util.Date;

public class Attendy {

	private int id;
	private String confServerId;
	private int confId;
	private String emailId;
	private Boolean attending;
	private Date createdDate;

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getConfServerId() {
		return confServerId;
	}
	public void setConfServerId(String confServerId) {
		this.confServerId = confServerId;
	}
	
	public int getConfId() {
		return confId;
	}
	public void setConfId(int confId) {
		this.confId = confId;
	}
	public Boolean getAttending() {
		return attending;
	}
	public void setAttending(String attending) {
		this.attending = Boolean.parseBoolean(attending);
	}
	
	
	
	
	
}
