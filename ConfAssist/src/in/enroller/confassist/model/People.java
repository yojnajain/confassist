package in.enroller.confassist.model;

public class People {

		private int id;
		private int confId;
		private int agendaId;
		private String speakerName;
		private String type;
		private String photo;
		private String tagLine;
		private String bio;
		private String serverId;
		
		
	
		public int getId() {
			return id;
		}
		public int getConfId() {
			return confId;
		}
		public void setConfId(int confId) {
			this.confId = confId;
		}
		public int getAgendaId() {
			return agendaId;
		}
		public void setAgendaId(int agendaId) {
			this.agendaId = agendaId;
		}
		public String getSpeakerName() {
			return speakerName;
		}
		public void setSpeakerName(String speakerName) {
			this.speakerName = speakerName;
		}
		public String getTagLine() {
			return tagLine;
		}
		public void setTagLine(String tagLine) {
			this.tagLine = tagLine;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getPhoto() {
			return photo;
		}
		public void setPhoto(String photo) {
			this.photo = photo;
		}
		
		
		public String getBio() {
			return bio;
		}
		public void setBio(String bio) {
			this.bio = bio;
		}
		public String getServerId() {
			return serverId;
		}
		public void setServerId(String serverId) {
			this.serverId = serverId;
		}
		
		
}
