package in.enroller.confassist;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.jsonserialization.JsonToSQLiteDB;
import in.enroller.confassist.model.Conference;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.fedorvlasov.lazylist.ImageLoader;

public class HomeActivity extends Activity {
	ConferenceListAdapter custAd;
	ImageButton btnSearch;
	public static ImageLoader imageLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_home);
		ActionBar actionbar = getActionBar();
		/*
		 * Following is to load add_data() only once. We use shared preferences
		 * to store settings. ----
		 */

		DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
		SharedPreferences prefs = this.getSharedPreferences("in.enroller.confassist", Context.MODE_PRIVATE);
		if (!prefs.getBoolean("testDataLoaded", false)) {

			// db.add_data();
			/*
			 * try {
			 * 
			 * 
			 * newConf(); } catch (ParseException e) { // TODO Auto-generated
			 * catch block e.printStackTrace(); }
			 */
			prefs.edit().putBoolean("testDataLoaded", true).commit();
		}

		// Display date in list view
		if (imageLoader == null)
			imageLoader = new ImageLoader(getApplicationContext());

		final ListView lv = (ListView) findViewById(R.id.listConf);
		List<Conference> lt = db.getAllConferences();
		custAd = new ConferenceListAdapter(this, lt);
		lv.setAdapter(custAd);
		lv.setSelection(0);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent i = new Intent(HomeActivity.this, ConferenceActivity.class);
				Conference co = (Conference) view.getTag();

				i.putExtra("ID", "" + co.getId());

				startActivity(i);

			}

		});

	}

	public void newConf() throws ParseException {

		Conference conf = new Conference();
		conf.setId(12);
		conf.setShortCode("3456");
		conf.setTitle("New Conf");
		conf.setDescription("For checking purpose");
		conf.setEmail("yojna1992@gmail.com");
		conf.setLongitude("23.190863");
		conf.setLatitude("-6.152344");
		conf.setCreator("yojna1992@gmail.com");
		conf.setWebsite("http://www.google.com");
		conf.setLogo("http://upload.wikimedia.org/wikipedia/en/d/d8/PNG_logo_small.png");
		Date dt = null;
		dt = (Date) Utilities.sdfFull.parse("2014-10-07 09:00:00");
		conf.setStartDate(dt);
		dt = (Date) Utilities.sdfFull.parse("2014-10-07 09:00:00");
		conf.setEndDate(dt);
		conf.setVenue("Ahmedabad");
		conf.setIsAgendaFinal("true");

		JsonToSQLiteDB j = new JsonToSQLiteDB(getApplicationContext());
		j.ConfToSQL(conf, getApplicationContext());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.action_settings:
			Toast.makeText(this, "setting selected", Toast.LENGTH_SHORT).show();

			Intent iset = new Intent(HomeActivity.this, SettingScreenActivity.class);
			startActivity(iset);

			break;
		// action with ID action_settings was selected
		case R.id.action_about:
			Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(HomeActivity.this, AboutScreenActivity.class);
			startActivity(i);
			break;
		case R.id.action_search:
			Intent searchConf = new Intent(HomeActivity.this, SearchConf.class);
			startActivity(searchConf);
			break;
		default:
			break;
		}

		// return true;
		return super.onOptionsItemSelected(item);
	}

}