package in.enroller.confassist;

//#7
import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.jsonserialization.DeleteLikeToURL;
import in.enroller.confassist.jsonserialization.PostAttendyToURL;
import in.enroller.confassist.jsonserialization.PostLikeInterestToURL;
import in.enroller.confassist.model.Attendy;
import in.enroller.confassist.model.Conference;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.test.UiThreadTest;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ConferenceActivity extends Activity {
	ImageView iv;
	TextView confId, date, confCode;
	TextView detail, venue;
	ImageView website, email, phoneNo;
	Button bTalk, bSpeaker;
	ImageView image_venue, calender;

	String id, str;
	int i;
	static SimpleDateFormat sdf;
	long StarttimeInMilliseconds, EndtimeInMilliseconds;
	Date StartDate, EndDate;

	DatabaseAdapter da;
	static Conference conf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_conference);
		ActionBar actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);

		iv = (ImageView) findViewById(R.id.imageView1);

		// like = (ImageView) findViewById(R.id.buttonLikeConf);
		// like.setTag("empty");
		confId = (TextView) findViewById(R.id.textView1);
		date = (TextView) findViewById(R.id.textView2);
		confCode = (TextView) findViewById(R.id.textView3);

		detail = (TextView) findViewById(R.id.textView4);
		venue = (TextView) findViewById(R.id.textView5);

		website = (ImageView) findViewById(R.id.website);
		email = (ImageView) findViewById(R.id.email);
		phoneNo = (ImageView) findViewById(R.id.phone);

		bTalk = (Button) findViewById(R.id.button1);
		bSpeaker = (Button) findViewById(R.id.button2);

		image_venue = (ImageView) findViewById(R.id.image_venue);
		calender = (ImageView) findViewById(R.id.calender_icon);

		Bundle b = this.getIntent().getExtras();
		id = b.getString("ID");
		i = Integer.parseInt(id);

		da = new DatabaseAdapter(this);
		conf = da.getConfRecord(i);

		confId.setText(conf.getTitle());
		confCode.setText("#" + conf.getShortCode());
		detail.setText(conf.getDescription());

		date.setText(Utilities.sdfPrintFormat.format(conf.getStartDate()));

		// venue
		venue.setText(conf.getVenue());
		if (conf.getPhoneNo() == null) {

			phoneNo.setVisibility(View.GONE);
		} else {
			phoneNo.setVisibility(View.VISIBLE);
		}
		
		image_venue.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String map = "http://maps.google.co.in/maps?q=" + conf.getVenue();
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
				startActivity(i);
			}
		});

		// Website
		website.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Uri uriUrl = Uri.parse(conf.getWebsite());
				Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
				startActivity(launchBrowser);

			}
		});
		if (conf.getWebsite() == null || conf.getWebsite() == " ") {
			website.setVisibility(View.GONE);
		} else {
			website.setVisibility(View.VISIBLE);
		}

		// E-mail
		email.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.setType("plain/text");
				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { conf.getEmail() });
				startActivity(Intent.createChooser(emailIntent, ""));
			}
		});

		if (conf.getEmail() == null) {
			email.setVisibility(View.GONE);
		} else {
			email.setVisibility(View.VISIBLE);
		}

		if (conf.getLogo() != null || conf.getLogo() != " ")
			HomeActivity.imageLoader.DisplayImage(conf.getLogo(), iv);

		// Ph.No.:
		str = conf.getPhoneNo();

		phoneNo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent dialintent = new Intent(Intent.ACTION_DIAL);
				dialintent.setData(Uri.parse("tel:" + str));
				startActivity(dialintent);
			}
		});

		// calender
		calender.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					StartDate = conf.getStartDate();
					EndDate = conf.getEndDate();

				} catch (Exception e) {
					e.printStackTrace();
				}
				long StarttimeInMilliseconds = StartDate.getTime();
				long EndtimeInMilliseconds = EndDate.getTime();

				if (Build.VERSION.SDK_INT >= 14) {
					Intent intent = new Intent(Intent.ACTION_INSERT).setData(Events.CONTENT_URI).putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, StarttimeInMilliseconds).putExtra(CalendarContract.EXTRA_EVENT_END_TIME, EndtimeInMilliseconds).putExtra(Events.TITLE, conf.getTitle()).putExtra(Events.DESCRIPTION, conf.getDescription()).putExtra(Events.EVENT_LOCATION, conf.getVenue()).putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY).putExtra(Intent.EXTRA_EMAIL, conf.getEmail());
					startActivity(intent);
				}

				else {
					Intent intent = new Intent(Intent.ACTION_EDIT);
					intent.setType("vnd.android.cursor.item/event");
					intent.putExtra("beginTime", StarttimeInMilliseconds);
					intent.putExtra("allDay", true);
					intent.putExtra("rrule", "FREQ=YEARLY");
					intent.putExtra("endTime", EndtimeInMilliseconds);
					intent.putExtra("title", conf.getTitle());
					startActivity(intent);
				}
			}
		});
		// Talk Button
		detail.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

		bTalk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(ConferenceActivity.this, TalkListActivity.class);
				Bundle b1 = new Bundle();
				b1.putString("ID", id);
				i.putExtras(b1);
				startActivity(i);
			}
		});
		// Speaker Button
		bSpeaker.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(ConferenceActivity.this, ListConfSpeakersActivity.class);
				Bundle b1 = new Bundle();
				b1.putString("ID", id);
				i.putExtras(b1);
				startActivity(i);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conference, menu);

		Bundle b = this.getIntent().getExtras();
		id = b.getString("ID");
		i = Integer.parseInt(id);

		da = new DatabaseAdapter(this);
		conf = da.getConfRecord(i);
		int num = da.fetchAttendy(Utilities.findEmail(this), conf.getServerId());
		if (num == 0) {
			menu.removeItem(R.id.action_attendy);

		} else {
			menu.removeItem(R.id.action_notAttendy);

		}
		if (conf.getLike().equals("true")) {
			menu.removeItem(R.id.action_likeEmpty);

		} else {
			menu.removeItem(R.id.action_likeFilled);
		}

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.action_settings:
			Toast.makeText(this, "setting selected", Toast.LENGTH_SHORT).show();

			Intent iset = new Intent(ConferenceActivity.this, SettingScreenActivity.class);
			startActivity(iset);

			break;
		// action with ID action_settings was selected
		case R.id.action_about:
			Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(ConferenceActivity.this, AboutScreenActivity.class);
			startActivity(i);
			break;
		case android.R.id.home:
			Intent ia = new Intent(ConferenceActivity.this, HomeActivity.class);
			startActivity(ia);
			break;
		case R.id.action_notAttendy:
			mark();
			recreate();
			break;
		case R.id.action_attendy:
			break;
		case R.id.action_likeEmpty:
			likeUnlikeConf("true");			
			recreate();
			break;
		case R.id.action_likeFilled:
			likeUnlikeConf("false");
			recreate();
			break;
		default:
			break;
		}

		// return true;
		return super.onOptionsItemSelected(item);
	}

	void mark() {
		Bundle b = this.getIntent().getExtras();
		id = b.getString("ID");
		i = Integer.parseInt(id);
		Log.i("called", "call");
		da = new DatabaseAdapter(this);
		conf = da.getConfRecord(i);
		ContentValues v = new ContentValues();
		v.put("confId", "" + conf.getId());
		v.put("confServerId", conf.getServerId());
		v.put("attending", "true");
		v.put("createdDate", Utilities.sdfFull.format(new Date()));
		v.put("emailId", Utilities.findEmail(getApplicationContext()));
		Log.i("Attendy", "" + v);
		DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
		db.saveAttendy(v);
		Log.i("Attendy post method", "Attendy posted on Web service");
	}

	void likeUnlikeConf(String like) {
		Utilities u = new Utilities();
		if (u.checkInternetConnectivity(getApplicationContext())) {
			String emailId = Utilities.findEmail(getApplicationContext());

			if (like.equals("true")) {
				JSONObject obj = new JSONObject();
				try {

					obj.put("confServerId", conf.getServerId());
					obj.put("emailId", emailId);
					DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
					db.updateLikeFromConference(conf.getServerId(), "true");
					PostLikeInterestToURL plttu = new PostLikeInterestToURL(obj);
					plttu.execute();
					Toast.makeText(getApplicationContext(), "Successfully liked. ", Toast.LENGTH_SHORT).show();

				} catch (JSONException e) {
					Log.e("Like Server Error", "", e);
					Toast.makeText(getApplicationContext(), "Error occured in saving your Like! Make sure internet is available. ", Toast.LENGTH_LONG).show();
				}

			} else if (like.equals("false")) {

				JSONObject obj = new JSONObject();
				try {

					obj.put("confServerId", conf.getServerId());
					obj.put("emailId", emailId);
					DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
					db.updateLikeFromConference(conf.getServerId(), "false");
					DeleteLikeToURL deleteLike = new DeleteLikeToURL(obj);
					deleteLike.execute();
					Toast.makeText(getApplicationContext(), "Successfully unliked. ", Toast.LENGTH_SHORT).show();
				} catch (JSONException e) {
					Log.e("Like Server Error", "", e);
					Toast.makeText(getApplicationContext(), "Error occured in saving your Like! Make sure internet is available. ", Toast.LENGTH_LONG).show();
				}
			}
		} else {
			Toast.makeText(getApplicationContext(), "Please make sure your internet is connected.", Toast.LENGTH_LONG).show();

		}

	}

}