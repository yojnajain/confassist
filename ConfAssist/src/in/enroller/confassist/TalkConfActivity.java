package in.enroller.confassist;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.Conference;
import in.enroller.confassist.model.People;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

public class TalkConfActivity extends Activity {

	TextView talkTitle,tdesc,tvenue,tdate, ttime, ConfName, title;
	ImageView imlogo;
	DatabaseAdapter db;
	ListView speakerList;
	Button feedback;
	static SimpleDateFormat fmt;
	String id;
	int i;
	Agenda agd;
	Conference conf;

	long StarttimeInMilliseconds,EndtimeInMilliseconds;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_talk_conf);
		ActionBar actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		
		imlogo=(ImageView)findViewById(R.id.conflogo);		
		talkTitle=(TextView)findViewById(R.id.talkTitle);
		tdesc=(TextView)findViewById(R.id.tdesc);
		tdate=(TextView)findViewById(R.id.textViewDate);
		ttime=(TextView) findViewById(R.id.textView4);
		tvenue=(TextView)findViewById(R.id.textView9);
		ConfName = (TextView)findViewById(R.id.ConferenceName);
		db = new DatabaseAdapter(this);
		
	Bundle b = this.getIntent().getExtras();
		 id = b.getString("conferenceID");
		 Log.i("I = ",id);
		int conferenceID = Integer.parseInt(id);
		 id = b.getString("agendaID");
		  	int agendaID = Integer.parseInt(id);
								
			 conf = db.getConfRecord(conferenceID);
			 ConfName.setText(conf.getTitle());
			this.setTitle(conf.getTitle());
			List<Agenda> agenda = conf.getAgendas();
		
			int j=0;
			while(j<= agenda.size())
			{
			  agd = agenda.get(j++);
			 if(agd.getId()==agendaID)
				 break;
			
			}
			talkTitle.setText(agd.getTitle());
			tdesc.setText(agd.getAbstractOfTalk());
			tvenue.setText(agd.getVenue());
			
		 HomeActivity.imageLoader.DisplayImage(conf.getLogo(), imlogo);
		 
		List<People> people = agd.getPeople();
		speakerList = (ListView) findViewById(R.id.listView1);
		SpeakerListAdapter adapt = new SpeakerListAdapter(getApplicationContext(), people);
		speakerList.setAdapter(adapt);
		speakerList.setSelection(0);
	
		Date dt=agd.getStartTime();	
	Log.i("Duration",""+agd.getDuration());
		ttime.setText(Utilities.sdfTime.format(dt) +", "+agd.getDuration()/60 + " minutes");
		  
		tdate.setText(Utilities.sdfPrintFormat.format(dt));
		/*	feedback.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Intent i = new Intent(TalkConfActivity.this, TalkFeebackActivity.class);
					i.putExtra("confID",""+conf.getId());
					Log.i("ID", ""+agd.getId() + "  "+conf.getId());
					i.putExtra("agendaID", ""+agd.getId());
					startActivity(i);
				}
			});
	*/
		
 }
		   	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.talk_conf, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.action_settings:
			Toast.makeText(this, "setting selected", Toast.LENGTH_SHORT).show();

			Intent iset = new Intent(TalkConfActivity.this, SettingScreenActivity.class);
			startActivity(iset);

			break;
		// action with ID action_settings was selected
		case R.id.action_about:
			Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(TalkConfActivity.this, AboutScreenActivity.class);
			startActivity(i);
			break;
		case android.R.id.home:
			Intent ia = new Intent(TalkConfActivity.this, HomeActivity.class);
			startActivity(ia);
			break;
		case R.id.action_feedback:
			Intent ib = new Intent(TalkConfActivity.this, TalkFeebackActivity.class);
			ib.putExtra("confID",""+conf.getId());
			Log.i("ID", ""+agd.getId() + "  "+conf.getId());
			ib.putExtra("agendaID", ""+agd.getId());
			startActivity(ib);
		default:
			break;
		}

		// return true;
		return super.onOptionsItemSelected(item);
	}
}
