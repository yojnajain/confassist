package in.enroller.confassist;

import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.Conference;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class TalkFeebackActivity extends Activity {
	RatingBar ratingbar;
	Agenda agd = null;
	TextView confname, talkname, postedComment, title;
	EditText comment;
	ImageView logo;
	Conference conf;
	ImageButton post;
	Float rating;
	String possibleEmail;

	// Button feedback;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_talk_feeback);
		ActionBar actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);

		ratingbar = (RatingBar) findViewById(R.id.ratingBar2);
		confname = (TextView) findViewById(R.id.ConfName);
		talkname = (TextView) findViewById(R.id.textViewTalkTitle);
		postedComment = (TextView) findViewById(R.id.textViewPostedComment);
		comment = (EditText) findViewById(R.id.editTextComment);
		logo = (ImageView) findViewById(R.id.conflogo);
		post = (ImageButton) findViewById(R.id.buttonPost);
		title = (TextView) findViewById(R.id.TitleBarTextView);
		// feedback = (Button) findViewById(R.id.buttonFeedback);

		// feedback.setVisibility(2);
		DatabaseAdapter db = new DatabaseAdapter(this);
		Bundle b = this.getIntent().getExtras();
		String id = b.getString("confID");
		Log.i("I = ", id);
		int conferenceID = Integer.parseInt(id);
		id = b.getString("agendaID");
		int agendaID = Integer.parseInt(id);

		conf = db.getConfRecord(conferenceID);
		confname.setText(conf.getTitle());
		HomeActivity.imageLoader.DisplayImage(conf.getLogo(), logo);

		List<Agenda> agenda = conf.getAgendas();

		int j = 0;
		while (j <= agenda.size()) {
			agd = agenda.get(j++);

			if (agd.getId() == agendaID)
				break;

		}
		Log.i("ID", "" + agd.getId() + "  " + conf.getId());
		Log.i("ag", "" + agd);

		talkname.setText(agd.getTitle());

		this.setTitle(agd.getTitle());

		ratingbar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float ratings, boolean fromUser) {
				// TODO Auto-generated method stub

				rating = ratings;
				// Log.i("Ratings", ""+rating);
			}
		});
		post.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
				ContentValues v_rating = new ContentValues();
				v_rating.put("rating", rating);
				Date d = new Date();
				Log.i("Server id", "" + agd.getServerId());
				v_rating.put("createdDate", "" + d);
				v_rating.put("agendaId", agd.getId());
				v_rating.put("feedback", comment.getText().toString());
				v_rating.put("agendaServerId", agd.getServerId());

				Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
				Account[] accounts = AccountManager.get(getApplicationContext()).getAccounts();
				for (Account account : accounts) {
					if (emailPattern.matcher(account.name).matches()) {
						possibleEmail = account.name;
						v_rating.put("emailId", possibleEmail);
					}

				}
				v_rating.put("userName", "");
				v_rating.put("emailId", possibleEmail);

				db.setRatings(v_rating, agd.getId(), d, agd.getServerId());

				Toast.makeText(getApplicationContext(), "Feedback & rating Saved.", Toast.LENGTH_LONG).show();

				Intent i = new Intent(TalkFeebackActivity.this, TalkConfActivity.class);

				i.putExtra("conferenceID", "" + conf.getId());
				i.putExtra("agendaID", "" + agd.getId());
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.talk_feeback, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.action_settings:
			Toast.makeText(this, "setting selected", Toast.LENGTH_SHORT).show();

			Intent iset = new Intent(TalkFeebackActivity.this, SettingScreenActivity.class);
			startActivity(iset);

			break;
		// action with ID action_settings was selected
		case R.id.action_about:
			Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(TalkFeebackActivity.this, AboutScreenActivity.class);
			startActivity(i);
			break;
		case android.R.id.home:
			Intent ia = new Intent(TalkFeebackActivity.this, HomeActivity.class);
			startActivity(ia);
			break;
		default:
			break;
		}

		// return true;
		return super.onOptionsItemSelected(item);
	}
}
