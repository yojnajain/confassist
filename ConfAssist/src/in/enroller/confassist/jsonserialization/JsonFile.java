package in.enroller.confassist.jsonserialization;
import in.enroller.confassist.model.Conference;
import in.enroller.confassist.model.Feedback;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

public class JsonFile {
	
	String fileName = "temp.txt";
	File myDir,file_new ;
	Context context;
	public JsonFile()
	{	
		
	}
	public JsonFile(Context context) {
		context = this.context;
	}

	FileOutputStream fos;
	
	
	
	public void writeToFile(Context context , Conference conf) throws IOException
	{
	     String s = objectToJsonFile(conf);
         File myFile = new File(context.getFilesDir(),fileName);
      
      if(!myFile.exists())
      {
       	Boolean b = myFile.createNewFile();     
      	fos = context.openFileOutput(fileName, context.MODE_PRIVATE);
    	OutputStreamWriter osw = new OutputStreamWriter(fos);
           
              osw.write(s);
              osw.flush();
              osw.close();
             
    	  if(fos!= null)
    		  fos.close();
      }
	}
	
	public String objectToJsonFile(Conference conf){
		
		 Gson gson = new Gson();
	     return gson.toJson(conf);
	}
	
	public String FeedbackObjectTOJson(Feedback feedback)
	{		
		Gson gson = new Gson();
		Log.i("Post json", gson.toJson(feedback));
		return gson.toJson(feedback);
	}

	
	
}