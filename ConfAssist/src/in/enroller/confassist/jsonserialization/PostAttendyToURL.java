package in.enroller.confassist.jsonserialization;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.model.Attendy;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PostAttendyToURL extends AsyncTask<String, Void, Void> {
	Attendy attendy;

	public PostAttendyToURL(Attendy attendy) {
		super();
		this.attendy = attendy;
	}

	@Override
	protected Void doInBackground(String... params) {

		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostRequest = new HttpPost("http://confassistwebservice.herokuapp.com/api/attendy");
			Gson gson = new GsonBuilder().setDateFormat(Utilities.sdfFull.toPattern()).create();
			StringEntity se = new StringEntity(gson.toJson(attendy));
			httpPostRequest.setEntity(se);
			Log.i("attendy json ", gson.toJson(attendy));
			httpPostRequest.setHeader("Accept", "application/json;charset=UTF-8");
			httpPostRequest.setHeader("Content-Type", "application/json;charset=UTF-8");
			HttpResponse httpResponse = httpClient.execute(httpPostRequest);
			Log.i("Response", EntityUtils.toString(httpResponse.getEntity()));
		} catch (ClientProtocolException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
