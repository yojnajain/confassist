package in.enroller.confassist.jsonserialization;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class DeleteLikeToURL extends AsyncTask<String, Void, Void> {
	JSONObject obj;

	public DeleteLikeToURL(JSONObject obj) {
		super();
		this.obj = obj;
	}

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		DefaultHttpClient httpClient = new DefaultHttpClient();

		HttpDelete httpDeleteRequest = null;
		try {
			httpDeleteRequest = new HttpDelete("http://confassistwebservice.herokuapp.com/api/like/"
					+ obj.getString("confServerId") + "/" + obj.getString("emailId"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		httpDeleteRequest.setHeader("Accept", "application/json;charset=UTF-8");
		httpDeleteRequest.setHeader("Content-Type", "application/json;charset=UTF-8");
		HttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(httpDeleteRequest);
			Log.i("Like Response ", EntityUtils.toString(httpResponse.getEntity()));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
