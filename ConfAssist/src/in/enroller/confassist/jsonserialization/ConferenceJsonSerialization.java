package in.enroller.confassist.jsonserialization;

import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.Conference;
import in.enroller.confassist.model.People;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConferenceJsonSerialization {
	/*final String create_conf_table = "CREATE TABLE conference (_id INTEGER PRIMARY KEY, short_code TEXT, "
			+ "title TEXT, description TEXT, logo TEXT, venue TEXT, geo TEXT, website TEXT, email TEXT, phone_no NUMBER, "
			+ "start_date DATE, end_date DATE, is_people_final BOOLEAN)";
	
	final String create_agenda_table = "CREATE TABLE agenda (_id INTEGER PRIMARY KEY, conf_id NUMERIC, "
				+ "title TEXT, abstract TEXT, venue TEXT, start_time DATE, duration NUMERIC)";

final String create_conf_speaker_table = "CREATE TABLE people (_id INTEGER PRIMARY KEY, agenda_id NUMERIC, "
				+ "conf_id NUMERIC,type TEXT, photo TEXT, tag_line TEXT, bio TEXT, name TEXT)";
		
*/
	final String CREATOR = "creator";
	final String ID = "_id";
	final String SHORT_CODE ="short_code";
	final String CONF_TITLE = "title";
	final String DESCRIPTION = "desc";
	final String LOGO = "logo";
	final String VENUE = "venue";
	final String LONGITUDE = "longitude";
	final String LATITUDE = "latitude";
	final String WEBSITE = "website";
	final String EMAIL = "email";
	final String PHONE_NO = "phone_no";
	final String START_DATE = "start_date";
	final String END_DATE = "end_date";
	final String IS_AGENDA_FINAL = "is_people_final";
	
	
	JSONObject jobject;
	JSONArray jsonArray;
	
	public JSONObject toJsonObject(final Conference conference) throws JSONException
	{
		jobject = new JSONObject();
	
			jobject.put(CREATOR, conference.getCreator());
			jobject.put(ID, conference.getId());
			jobject.put(SHORT_CODE, conference.getShortCode());
			jobject.put(CONF_TITLE, conference.getTitle());
			jobject.put(DESCRIPTION, conference.getDescription());
			jobject.put(LOGO, conference.getLogo());
			jobject.put(VENUE, conference.getVenue());
			jobject.put(LATITUDE, conference.getLatitude());
			jobject.put(LONGITUDE, conference.getLongitude());
			jobject.put(WEBSITE, conference.getWebsite());
			jobject.put(EMAIL, conference.getEmail());
			jobject.put(PHONE_NO, conference.getPhoneNo());
			jobject.put(START_DATE, conference.getStartDate());
			jobject.put(END_DATE, conference.getEndDate());
			jobject.put(IS_AGENDA_FINAL, conference.getIsAgendaFinal());
		
		ArrayList<Agenda> agenda = (ArrayList<Agenda>) conference.getAgendas();
		Agenda agd;
		JSONArray aa = null;
		
				for(int i=0;i!=agenda.size();i++)
				{
					agd = agenda.get(i);
					aa.put(agendaToJsonObject(agd));
					
					
				}
				jobject.put("agendas", aa);
		return jobject;
	}
	
	public void fromJsonObject(final JSONObject jsobj) throws ParseException, JSONException
	{
			Conference conference = new Conference();
			
				conference.setCreator(jsobj.getString(CREATOR));
				conference.setId(jsobj.getInt(ID));
				conference.setTitle(jsobj.getString(CONF_TITLE));
				conference.setShortCode(jsobj.getString(SHORT_CODE));
				conference.setDescription(jsobj.getString(DESCRIPTION));
				conference.setLogo(jsobj.getString(LOGO));
				conference.setVenue(jsobj.getString(VENUE));
				conference.setLatitude(jsobj.getString(LATITUDE));
				conference.setLongitude(jsobj.getString(LONGITUDE));
				conference.setWebsite(jsobj.getString(WEBSITE));
				conference.setEmail(jsobj.getString(EMAIL));
				conference.setPhoneNo(jsobj.getString(PHONE_NO));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				
				String dt = jsobj.getString(START_DATE);
				conference.setStartDate((Date)sdf.parse(dt));
				
				conference.setEndDate(sdf.parse(jsobj.getString(END_DATE)));
				conference.setIsAgendaFinal(jsobj.getString(IS_AGENDA_FINAL));
			
				JSONArray aa = jobject.getJSONArray("agendas");
				Agenda agd = null;
				ArrayList<Agenda> agenda = new ArrayList<Agenda>();
				for(int i=0;i<aa.length();i++)
				{
					agd = convertJsonToAgenda(aa.getJSONObject(i));
					agenda.add(agd);
							
				}
						
				conference.setAgendas(agenda);
		
		
	}
	public Agenda convertJsonToAgenda(JSONObject jobj) throws NumberFormatException, JSONException, ParseException
	{
		Agenda agenda = new Agenda();
		agenda.setServerId(jobj.getString("serverId"));
		//agenda.setConfId(Integer.parseInt(jobj.getString("conf_id")));
		agenda.setTitle(jobj.getString("title"));
		agenda.setAbstractOfTalk(jobj.getString("abstract"));
		agenda.setVenue(jobj.getString("venue"));
	
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		agenda.setStartTime(Utilities.sdfFull.parse(jobj.getString("start_time")));
		agenda.setDuration(Integer.parseInt(jobj.getString("duration"))); // in minutes
		
		JSONArray aa = jobject.getJSONArray("people");
		People pl = null;
		ArrayList<People> people = new ArrayList<People>();
		for(int i=0;i<aa.length();i++)
		{
			pl = convertJsonToPeople(aa.getJSONObject(i));
			people.add(pl);
					
		}
				
		agenda.setPeople(people);
		return agenda;
	}
	public People convertJsonToPeople(JSONObject jobj) throws NumberFormatException, JSONException, ParseException
	{
		People people = new People();
		people.setServerId((jobj.getString("serverId")));
		//people.setConfId(Integer.parseInt(jobj.getString("conf_id")));
		//people.setAgendaId(Integer.parseInt(jobj.getString("agenda_id")));
		people.setType(jobj.getString("type"));
		people.setPhoto(jobj.getString("photo"));
		people.setSpeakerName(jobj.getString("name"));
		people.setTagLine(jobj.getString("tagLine"));
		people.setBio(jobj.getString("bio"));
		
		return people;
	}
	
	public JSONObject agendaToJsonObject(final Agenda agenda) throws JSONException
	{
		jobject = new JSONObject();
	
			jobject.put(ID, agenda.getId());
			jobject.put("conf_id", agenda.getConfId());
			jobject.put("title", agenda.getTitle());
			jobject.put("abstract", agenda.getAbstractOfTalk());
			jobject.put("venue", agenda.getVenue());
			jobject.put("star_time", agenda.getStartTime());
			jobject.put("duration", agenda.getDuration());
			
			ArrayList<People> people = (ArrayList<People>) agenda.getPeople();
			People pl;
			JSONArray aa = null;
			for(int i=0;i!=people.size();i++)
			{
				
				pl= people.get(i);
				aa.put(peopleToJsonObject(pl));
			}
			jobject.put("people", aa);
		
			return jobject;
	}
	public JSONObject peopleToJsonObject(final People people) throws JSONException
	{
		jobject = new JSONObject();
	
			jobject.put(ID, people.getId());
			jobject.put("conf_id", people.getConfId());
			jobject.put("agenda_id", people.getAgendaId());
			jobject.put("name", people.getSpeakerName());
			jobject.put("type", people.getType());
			jobject.put("bio", people.getBio());
			jobject.put("tag_line", people.getTagLine());
			jobject.put("photo", people.getPhoto());
			
			
		return jobject;
	}
}
