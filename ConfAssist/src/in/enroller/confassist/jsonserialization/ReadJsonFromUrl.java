package in.enroller.confassist.jsonserialization;

import in.enroller.confassist.model.Conference;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class ReadJsonFromUrl extends  AsyncTask<String, Void, Void> {

	Context context;
	String shortCode;
	
	
	public ReadJsonFromUrl(Context context, String shortCode) {
		super();
		this.context = context;
		this.shortCode = shortCode;
	}

	@Override
	protected Void doInBackground(String... params) {
		
		DefaultHttpClient defaultClient = new DefaultHttpClient();
		// Setup the get request
	Log.i("shortcode",params[0]);
		HttpGet httpGetRequest = new HttpGet("http://confassistwebservice.herokuapp.com/api/conference/"+ shortCode);
		
		// Execute the request in the client
		HttpResponse httpResponse;
		try {
			httpResponse = defaultClient.execute(httpGetRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
			String s = reader.readLine();
			Log.i("String : ", s);
			Conference conference = new Gson().fromJson(s, Conference.class);
			//Log.i("Conf detail : ",""+new Gson().toJson(conference));
			JsonToSQLiteDB jtsb = new JsonToSQLiteDB(context);
			jtsb.ConfToSQL(conference, context);
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ReadJsonFromUrl() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReadJsonFromUrl(Context applicationContext) {
		// TODO Auto-generated constructor stub
		this.context= applicationContext;
	}

}
