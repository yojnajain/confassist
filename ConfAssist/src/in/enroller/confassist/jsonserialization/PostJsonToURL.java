package in.enroller.confassist.jsonserialization;

import in.enroller.confassist.model.Feedback;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

public class PostJsonToURL extends AsyncTask<String, Void, Void>
{
	Feedback feedback;
	
	public PostJsonToURL(Feedback feedback) {
		// TODO Auto-generated constructor stub
		super();
		this.feedback = feedback;
	}

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		try{
			DefaultHttpClient httpClient =  new DefaultHttpClient();
			Log.i("url","http://confassistwebservice.herokuapp.com/api/feedback?email="+feedback.getEmailId()+"&feedback="+feedback.getFeedback()+"&rating="+feedback.getRating()+"&agendaId="+feedback.getAgendaServerId());
			HttpPost httpPostRequest =  new HttpPost("http://confassistwebservice.herokuapp.com/api/feedback");//?email="+feedback.getEmailId()+"&feedback="+feedback.getFeedback()+"&rating="+feedback.getRating()+"&agendaId="+feedback.getAgendaServerId());
			Gson json = new Gson();
			StringEntity se = new StringEntity(	json.toJson(feedback));

		    //sets the post request as the resulting string
		    httpPostRequest.setEntity(se);
		    httpPostRequest.setHeader("Accept", "application/json;charset=UTF-8");
			httpPostRequest.setHeader("Content-Type", "application/json;charset=UTF-8");
			HttpResponse httpResponse;
			httpResponse =  httpClient.execute(httpPostRequest);
			
		}catch(ClientProtocolException e){
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
}
	


