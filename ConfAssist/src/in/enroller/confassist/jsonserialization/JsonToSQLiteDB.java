package in.enroller.confassist.jsonserialization;

import in.enroller.confassist.DatabaseAdapter;
import in.enroller.confassist.common.Utilities;
import in.enroller.confassist.model.Agenda;
import in.enroller.confassist.model.Conference;
import in.enroller.confassist.model.People;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

public class JsonToSQLiteDB extends DatabaseAdapter {
	DatabaseAdapter mdb;
	public JsonToSQLiteDB(Context context) {
		super(context);
	}
	
	public void ConfToSQL(Conference conference, Context context)
	{
		 mdb = new DatabaseAdapter(context);
		 
		//mdb.getWritableDatabase();
		ContentValues v = new ContentValues();
		v.put("creator", conference.getCreator());
		//v.put("_id", conference.getId());
		v.put("title",conference.getTitle());
		v.put("short_code",conference.getShortCode());
		v.put("description",conference.getDescription());
		v.put("logo",conference.getLogo());
		v.put("latitude",conference.getLatitude());
		v.put("longitude",conference.getLongitude());
		v.put("venue",conference.getVenue());
		v.put("email",conference.getEmail());
		v.put("start_date",Utilities.sdfFull.format(conference.getStartDate()));
		v.put("serverId", conference.getServerId());
		v.put("end_date",Utilities.sdfFull.format(conference.getEndDate()));
		v.put("is_agenda_final",conference.getIsAgendaFinal());
		v.put("like", "false");
	
		//Log.i("CV", ""+v);
		long confId = mdb.insertData(v, "conference");
		//Log.i("Conf : ", ""+confId);
		agendaToSQL((ArrayList <Agenda>)conference.getAgendas(), confId, context);

	}
	
	

	public void agendaToSQL(ArrayList<Agenda> agenda, long confId, Context context)
	{
		 mdb = new DatabaseAdapter(context);
		Agenda agd;
		for(int i=0;i!=agenda.size();i++)
		{
			agd = agenda.get(i);
			ContentValues v = new ContentValues();
			//v.put("_id", agd.getId());
			v.put("conf_id", confId);
			v.put("title", agd.getTitle());
			v.put("abstract", agd.getAbstractOfTalk());
			v.put("venue" , agd.getVenue());
			v.put("start_time", Utilities.sdfFull.format(agd.getStartTime()));
			v.put("duration", agd.getDuration()*60);
			v.put("serverId",  agd.getServerId());
			
			Log.i("CV", ""+v);
			long agdId = mdb.insertData(v, "agenda");
			//Log.i("Agenda : ",""+agdId);
			peopleToSQL((ArrayList<People>)agd.getPeople(), agdId, confId, context);
			
		}
					
	}
	
	public void peopleToSQL(ArrayList<People>  people, long agdId, long confId, Context context)
	{
		
		 mdb = new DatabaseAdapter(context);
		 People pl;
		for(int i=0;i!=people.size();i++)
			{
				pl = people.get(i);
				ContentValues v = new ContentValues();
				//v.put("_id", pl.getId());
				v.put("conf_id", confId);
				v.put("agenda_id", agdId);
				v.put("type", pl.getType());
				v.put("name", pl.getSpeakerName());
				v.put("bio", pl.getBio());
				v.put("tag_line" , pl.getTagLine());
				v.put("photo", pl.getPhoto());
				v.put("serverId", pl.getServerId());
				//Log.i("CV", ""+v);			
				long ppl = mdb.insertData(v, "people");
				//Log.i("people : ", ""+ppl);
			}
	}
}
