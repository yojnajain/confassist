package in.enroller.confassist.jsonserialization;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

public class PostLikeInterestToURL extends AsyncTask<String, Void, Void> {

	JSONObject obj;

	public PostLikeInterestToURL(JSONObject obj) {
		super();
		this.obj = obj;
	}

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub

		DefaultHttpClient httpClient = new DefaultHttpClient();

		HttpPut httpPutRequest = null;
		try {
			httpPutRequest = new HttpPut("http://confassistwebservice.herokuapp.com/api/like/"
					+ obj.getString("confServerId") + "/" + obj.getString("emailId"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Gson gson = new Gson();
		StringEntity se;
		try {
			se = new StringEntity(gson.toJson(obj));
			httpPutRequest.setEntity(se);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		httpPutRequest.setHeader("Accept", "application/json;charset=UTF-8");
		httpPutRequest.setHeader("Content-Type", "application/json;charset=UTF-8");
		HttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(httpPutRequest);
			Log.i("Like Response ", EntityUtils.toString(httpResponse.getEntity()));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
