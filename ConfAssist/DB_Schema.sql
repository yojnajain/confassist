BEGIN TRANSACTION;
CREATE TABLE agenda (id INTEGER PRIMARY KEY, conf_id NUMERIC, title TEXT, anstract TEXT, vanue TEXT, start_time DATE, duration NUMERIC);
CREATE TABLE conference (id INTEGER PRIMARY KEY, short_code TEXT,title TEXT, description TEXT, logo TEXT, vanue TEXT,geo TEXT, website TXT, email TEXT, phone_no NUMBER,start_date DATE, end_date DATE, is_ageda_final BOOLEAN);
CREATE TABLE people (id INTEGER PRIMARY KEY, agenda_id NUMERIC, conf_id NUMERIC,type TEXT, photo TEXT, tag_line TEXT, bio TEXT, name TEXT);
COMMIT;
