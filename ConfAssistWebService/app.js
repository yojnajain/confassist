// Starting file

var express = require("express");
var app =  express();
var bodyParser =  require("body-parser");
var config = require('config');
var mongoose = require('mongoose');
var Conf =  require('./models/Conference');
var conferenceController = require('./controllers/conference');
var agendaController = require('./controllers/agenda');
var peopleController = require('./controllers/people');
var feedbackController = require('./controllers/feedback');
var attendyController = require('./controllers/attendy');
var likeController = require('./controllers/like');

mongoose.connect(config.get('mongourl'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;
var router = express.Router();
router.get('/', function(req, res){
  res.json({message : ' welcome to Web Service'});
});

app.use('/api', router);

app.listen (port);
console.log("We are on port " +port);
Conf.find({_id : '541478947ac9ca100af25046'}, function(err, conf){

  if(err)
  {
    console.log("Error Occured : " +err);
  }
  if(conf)
  {
    console.log("Confs : " +conf);
  }
});

app.get('/api/conference', conferenceController.getAllConference);
app.get('/api/conference/:shortCode', conferenceController.getConference);
app.post('/api/conference', conferenceController.createConference);
app.delete('/api/conference/:id', conferenceController.removeConference);
app.put('/api/conference/:id', conferenceController.updateConference);

app.get('/api/agenda', agendaController.getAllAgendas);
app.post('/api/agenda', agendaController.createAgenda);
app.get('/api/agenda/:id', agendaController.getAgenda);
app.delete('/api/agenda/:id', agendaController.removeAgenda);
app.put('/api/agenda/:id', agendaController.updateAgenda);

app.get('/api/people', peopleController.getAllPeople);
app.post('/api/people', peopleController.insertPeople);
app.get('/api/people/:id', peopleController.getPeople);
app.delete('/api/people/:id', peopleController.removePeople);
app.put('/api/people/:id', peopleController.updatePeople);

app.get('/api/feedback', feedbackController.getAllFeedback);
app.post('/api/feedback', feedbackController.insertFeedback);
app.get('/api/feedback/:id', feedbackController.getFeedback);
app.delete('/api/feedback/:id', feedbackController.removeFeedback);
app.put('/api/feedback/:id', feedbackController.updateFeedback);

app.post('/api/attendy', attendyController.insertAttendy);
app.get('/api/attendy', attendyController.getAttendy);

app.put('/api/like/:id/:emailId',likeController.insertLike);
app.get('/api/like/:id',likeController.getLikeConference);
app.delete('/api/like/:id/:emailId',likeController.deleteLike);