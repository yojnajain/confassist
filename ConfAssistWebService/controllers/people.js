var People =  require('../models/People');


exports.getAllPeople = function(req, res, next)
{
  
  People.find(function(err, ppls){
                  
    if(err)
    {
      console.log("Error occured " + err);
    }
    if(ppls)
    {
          res.send(ppls);
    }
    else{
      console.log("Error");
    }
  });
  
};

exports.getPeople = function(req, res, next)
{
  People.findById(req.params.id, function(err, ppl){
  
    if(err)
    {
       console.log("Error occured " + err);
    }
    if(ppl)
    {
      res.send(ppl);
    }
    else
    {
      console.log("Error");
    }
  });

};

exports.removePeople =  function(req, res, next)
{  
  People.findById(req.params.id, function(err, people){
    if(err)
    {
      res.send(false);
    }
    if(people)
    {
      console.log(people);
      People.findById(req.params.id).remove().exec();
      res.send(true);
    }
  });
    
};

exports.updatePeople =  function(req, res, next)
{
  
  People.findById(req.params.id, function(err, people)
  {
    if(err)
    {
      res.send(false);
    }
    if(people)
    {
      people.confId = req.body.confId;
      people.speakerName = req.body.speakerName;
      people.tagLine = req.body.tagLine;
      people.photo = req.body.photo;
      people.type = req.body.type;
      people.bio = req.body.bio;
  
      people.save(function (err, result) {

            if (err) {
              res.send(false);
            } 
            if(result) {
              res.send(true);  
            }
      });
      
    }
  });
};

exports.insertPeople = function(req, res, next)
{
      var people= new People();
  
      people.confId = req.body.confId;
      people.speakerName = req.body.speakerName;
      people.tagLine = req.body.tagLine;
      people.photo = req.body.photo;
      people.type = req.body.type;
      people.bio = req.body.bio;
  
      people.save(function (err, result) {

            if (err) {
              res.send(false);
            } 
            if(result) {
              res.send(true);  
            }
      });
};