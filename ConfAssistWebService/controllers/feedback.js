var Feedback =  require('../models/Feedback');


exports.getAllFeedback = function(req, res, next)
{
  
  Feedback.find(function(err, feedbacks){
                  
    if(err)
    {
      console.log("Error occured " + err);
    }
    if(feedbacks)
    {
          res.send(feedbacks);
    }
    else{
      console.log("Error");
    }
  });
  
};

exports.getFeedback = function(req, res, next)
{
  Feedback.findById(req.params.id, function(err, feedback){
  
    if(err)
    {
       console.log("Error occured " + err);
    }
    if(feedback)
    {
      res.send(feedback);
    }
    else
    {
      console.log("Error");
    }
  });

};

exports.removeFeedback =  function(req, res, next)
{
  Feedback.findById(req.params.id, function(err, feedback){
    console.log(feedback);
    if(err)
    {
      res.send(false);
    }
    if(feedback)
    {
      Feedback.findById(req.params.id).remove().exec();
      res.send(true);
    }
  }); 
};



exports.createFeedback = function(req, res, next)
{
  var feedback = new Feedback();
  //  feedback.userName = req.body.userName;
  feedback.emailId = req.body.email;
  feedback.agendaId = req.body.agendaServerId;
  feedback.feedback = req.body.feedback;
  feedback.rating = req.body.rating;
  feedback.createdDate = req.body.createdDate;
  
  feedback.save(function (err, feed) {

    if (err) {
     res.send(false);
      
    } else {
      res.send(true);
    }
  });
  
};

exports.updateFeedback = function(req, res, next)
{
  Feedback.findById(req.params.id, function(err, feedback){
    if(err)
    {
      res.send(false);
    }
    if(feedback)
    {
      //  feedback.userName = req.body.userName;
      feedback.emailId = req.body.email;
      feedback.agendaId = req.body.agendaServerId;
      feedback.feedback = req.body.feedback;
      feedback.rating = req.body.rating;
      feedback.createdDate = req.body.createdDate;
  
      feedback.save(function (err, feed) {

        if (err) {
          res.send(false);
      
        } if(feed) {
          res.send(true);
        }
      });
    }
  });
};

exports.insertFeedback = function(req, res, next)
{
  var feedback = new Feedback();
      feedback.emailId = req.body.email;
      feedback.agendaId = req.body.agendaServerId;
      feedback.feedback = req.body.feedback;
      feedback.rating = req.body.rating;
      feedback.createdDate = req.body.createdDate;
  
      feedback.save(function (err, feed) {

        if (err) {
          res.send(false);
      
        } if(feed) {
          res.send(true);
        }
      });
};
