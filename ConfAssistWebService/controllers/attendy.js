var Attendy = require('../models/Attendy');

exports.insertAttendy = function(req, res, next)
{
  var att = new Attendy();
  att.confId = req.body.confServerId;
  att.attending = req.body.attending;
  att.emailId = req.body.emailId;
  att.createdDate = req.body.createdDate;
  
  att.save(function(err, attendy){
    if(err)
    { console.error(err);
      res.send(false);
      return;
    }

    if(attendy)
    {
      res.send(true);
    }
    else{
      res.send(false);
    }

  });
};

exports.getAttendy = function(req, res, next)
{
    Attendy.find(function(err, attendy){
    if(err)
    {
      console.error(err);
      res.send(false);
      return;
    }
    if(attendy)
    {
      res.send(attendy);
    }
    else{
      res.send(false);
    }
  });
};