var Agenda =  require('../models/Agenda');
var People = require('../models/People');

exports.getAllAgendas = function(req, res, next)
{
  
  Agenda.find(function(err, agds){
                  
    if(err)
    {
      console.log("Error occured " + err);
      res.send(false);
      return;
    }
    if(agds)
    {
      res.send(agds);
      return;
    }
    else{
      console.log("Error occured " + err);
      res.send(false);
      return;
    }
  });
  
};

exports.getAgenda = function(req, res, next)
{
  Agenda.findById(req.params.id, function(err, agd){
  
    if(err)
    {
       console.log("Error occured " + err);
      res.send(false);
      return;
    }
    if(agd)
    {
      res.send(agd);
      return;
    }
    else
    {
      console.log("Error occured " + err);
      res.send(false);
      return;
    }
  });

};

exports.removeAgenda =  function(req, res, next)
{
  Agenda.findById(req.params.id, function(err, agd){
    console.log(agd);
 
    if(err)
    {   res.send(false);
    }
    if(agd)
    {
      Agenda.findById(req.params.id).remove().exec();
      res.send(true);
    }
  
  });
 
};

exports.updateAgenda =  function(req, res, next)
{
  
  Agenda.findById(req.params.id, function(err, agenda)
  {
    if(err)
    {
      console.log("Error occured " + err);
      res.send(false);
      return;
    }
    if(agenda)
    {
      agenda.title = req.body.title;
      agenda.abstractOfTalk = req.body.abstractOfTalk;
      agenda.venue =  req.body.venue;
      agenda.startTime = req.body.startTime;
      agenda.duration =  req.body.duration;
     
      agenda.save(function (err, result) {

            if (err) {
              res.send(false);
              return;
            } else {
              res.send(true);
              return;
            }
      });
    }
  });
};

exports.createAgenda =  function(req, res, next)
{
  var people = new People();
  people.confId = req.params.id;
  people.speakerName = req.body.speaker[0].speakerName;
  people.tagLine = req.body.speaker[0].tagLine;
  people.photo = req.body.speaker[0].photo;
  people.type = req.body.speaker[0].type;
  people.bio = req.body.speaker[0].bio;
  
   var agenda = new Agenda();
      agenda.title = req.body.title;
      agenda.abstractOfTalk = req.body.abstractOfTalk;
      agenda.venue =  req.body.venue;
      agenda.startTime = req.body.startTime;
      agenda.duration =  req.body.duration;
      agenda.speaker.push(people);
  agenda.save(function (err, result) {

    if (err) {
      console.log("Error occured " + err);
      res.send(false);
      return;
    } else {
     people.save(function (err, res) {
        if (err) {
          console.log("Error occured " + err);
          res.send(false);
          return;
        } if(res) {
        
          res.send(true);
          return;
        }
      });

      console.log("Agenda ID", result._id);
      people.agendaId = result._id;
      people.save(function (err, re) {
        if (err) {
          console.log("Error occured " + err);
          res.send(false);
          return;
        }
        else{
          res.send(true);
          return;
        }
      });
    }

   
  });
};