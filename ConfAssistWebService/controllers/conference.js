var Conference =  require('../models/Conference');
var Agenda = require('../models/Agenda');
var People = require('../models/People');
var moment = require('moment');
exports.getAllConference = function(req, res, next)
{
  
  Conference.find(function(err, confs){
                  
    if(err)
    {
        console.log("Error occured " + err);
        res.send(false);
        return;
    }
    if(confs)
    {
      res.send(confs);
      return;
    }
    else{
      console.log("Error occured " + err);
      res.send(false);
      return;
    }
  });
  
};

exports.getConference = function(req, res, next)
{
  Conference.findOne({shortCode : req.params.shortCode}).exec( function (err, conf) {

    if(err) {
      res.status(404);
      return next();
    }
    if(conf) {
      //conf._id = conf.creator = conf.__v = undefined;
      var c = {};
      console.log(conf);
      c.serverId = conf._id;
      c.shortCode = conf.shortCode;
      c.title = conf.title;
      c.description = conf.description;
      c.logo = conf.logo;
      c.venue = conf.venue;
      c.latitude = conf.latitude;
      c.longitude = conf.longitude;
      c.website = conf.website;
      c.email = conf.email;
      c.phoneNo = conf.phoneNo;
      c.startDate = moment(conf.startDate).format("MMM DD, YYYY hh:mm:ss A");
      c.endDate = moment(conf.endDate).format("MMM DD, YYYY hh:mm:ss A");
      c.isAgendaFinal = conf.isAgendaFinal;
      c.creator = conf.creator;
      c.agendas = [];

      Agenda.find({
        conf: conf._id
      })
        .populate('speaker', undefined, 'people')
        .exec(function (err, agenda) {
         console.log("gfdfd" ,agenda);
          if (agenda) {
            for (var i = 0; i < agenda.length; i++) {
              var a1 = {};
              a1.serverId = agenda[i]._id;
              //a1.confId = agenda[i].conf;
              a1.title = agenda[i].title;
              a1.abstractOfTalk = agenda[i].abstractOfTalk;
              a1.venue = agenda[i].venue;
              a1.startTime = moment(agenda[i].startTime).format("MMM DD, YYYY hh:mm:ss A");
              a1.duration = agenda[i].duration;

              a1.people = [];
              var p1 = {};
              for (var j = 0; j < agenda[i].speaker.length; j++) {
                p1.serverId = agenda[i].speaker[j]._id;
                //p1.agendaId = agenda[i].speaker[j].agendaId;
                //p1.confId = agenda[i].speaker[j].confId;
                p1.speakerName = agenda[i].speaker[j].speakerName;
                p1.type = agenda[i].speaker[j].type;
                p1.photo = agenda[i].speaker[j].photo;
                p1.tagLine = agenda[i].speaker[j].tagLine;
                p1.bio = agenda[i].speaker[j].bio;
                //  console.log(p1);
                a1.people.push(p1);

              }
              c.agendas.push(a1);
              console.log("Agenda added : ", a1);      
            }
            console.log("-----",c);
          }
          res.send(c);
        });
    } else {
      res.status(404);
      return next();
    }
  });

};

exports.removeConference =  function(req, res, next)
{
  Conference.findById(req.params.id, function(err, conf){
    
    if(err)
    {
      console.log("Error occured " + err);
      res.send(false);
      return;
    }
    if(conf)
    {
      Conference.findById(req.params.id).remove().exec();
      res.send(true);
    }
    });    
};

exports.updateConference = function(req, res, next)
{
    
  var conference = new Conference();
  
  conference.isAgendaFinal = req.body.isAgendaFinal;  
  conference.endDate = req.body.endDate;
  conference.startDate = req.body.startDate;
  conference.phoneNo = req.body.phoneNo;
  conference.email = req.body.email;
  conference.website = req.body.website;
  conference.longitude = req.body.longitude;
  conference.latitute = req.body.latitude;
  conference.country = req.body.country;
  conference.state = req.body.state;
  conference.city = req.body.city;
  conference.venue = req.body.venue;
  conference.logo =  req.body.logo;
  conference.description = req.body.desription;
  conference.shortCode = req.body.shortCode;
  conference.title = req.body.title;
  conference._id =  req.params.id;
  console.log(conference);
  
  conference.save(function (err, result) {
    if(err)
    { 
     console.log("Error occured " + err);
      res.send(false);
      return;
    }else{
      res.send(true);
    }
  });
    
};
exports.createConference = function(req, res, next)
{
  var conference = new Conference();
  
  conference.isAgendaFinal = req.body.isAgendaFinal;
  conference.endDate = req.body.endDate;
  conference.startDate = req.body.startDate;
  conference.phoneNo = req.body.phoneNo;
  conference.email = req.body.email;
  conference.website = req.body.website;
  conference.longitude = req.body.longitude;
  conference.latitute = req.body.latitude;
  conference.country = req.body.country;
  conference.state = req.body.state;
  conference.city = req.body.city;
  conference.venue = req.body.venue;
  conference.logo =  req.body.logo;
  conference.description = req.body.desription;
  conference.shortCode = req.body.shortCode;
  conference.title = req.body.title;
  conference.creator = req.body.creator;
  console.log(conference);
  
  conference.save(function (err, result) {
    if(err)
    { 
     console.log("Error occured " + err);
      res.send(false);
      return;
      return;
    }else{
      res.send(true);
      return;
    }
  });
};