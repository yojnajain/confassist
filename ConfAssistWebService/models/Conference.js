var mongoose = require('mongoose');
var textSearch = require('mongoose-text-search');

var ConfSchema = new mongoose.Schema({

  creator: String,
  title: String,
  shortCode: {
    type: String,
    unique: true,
    lowercase: true,
    index: true
  },
  description: String,
  logo: String,
  venue: String,
  city: String,
  state: String,
  country: String,
  latitude: String,
  longitude: String,
  website: String,
  email: String,
  phoneNo: String,
  startDate: Date,
  endDate: Date,
  isAgendaFinal: String,
  like: [String]
});

// give our schema text search capabilities
ConfSchema.plugin(textSearch);

// add a text index to the title
ConfSchema.index({ title: 'text' });

module.exports = mongoose.model('conf', ConfSchema);