var mongoose = require('mongoose');
var AttendySchema = new mongoose.Schema({

  confId: String,
  attending: String,
  createdDate: Date,
  emailId: String
  
});

module.exports = mongoose.model('attendy',AttendySchema);