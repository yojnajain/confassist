var mongoose = require("mongoose");


var PeopleSchema = new mongoose.Schema({
  confId: String,
  agendaId: String,
  speakerName: String,
  type: String,
  photo: String,
  tagLine: String,
  bio: String
});
module.exports= mongoose.model('people',PeopleSchema);