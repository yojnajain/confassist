var mongoose =  require("mongoose");

var FeedbackSchema = new mongoose.Schema({
  userName: String,
  emailId: String,
  agendaId: String, // ServerId in json format
  feedback: String,
  rating: Number,
  createdDate: Date
});

module.exports = mongoose.model('feedback', FeedbackSchema);