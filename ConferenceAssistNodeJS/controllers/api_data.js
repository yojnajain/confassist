var Conference = require('../models/Conference');
var Agenda = require('../models/Agenda');
var People = require('../models/People');
var moment = require('moment');
exports.getConferenceJson = function (req, res, next) {

  Conference.findOne({shortCode : req.params.shortCode}).exec( function (err, conf) {

    if (err) {
      res.status(404);
      return next();
    }
    if (conf) {
      //conf._id = conf.creator = conf.__v = undefined;
      var c = {};
      console.log(conf);
      c.serverId = conf._id;
      c.shortCode = conf.shortCode;
      c.title = conf.title;
      c.description = conf.description;
      c.logo = conf.logo;
      c.venue = conf.venue;
      c.latitude = conf.latitude;
      c.longitude = conf.longitude;
      c.website = conf.website;
      c.email = conf.email;
      c.phoneNo = conf.phoneNo;
      c.startDate = moment(conf.startDate).format("MMM DD, YYYY hh:mm:ss A");
      c.endDate = moment(conf.endDate).format("MMM DD, YYYY hh:mm:ss A");
      c.isAgendaFinal = conf.isAgendaFinal;
      c.agendas = [];

      Agenda.find({
        conf: conf._id
      })
        .populate('speaker', undefined, 'people')
        .exec(function (err, agenda) {
         console.log("gfdfd" ,agenda);
          if (agenda) {
            for (var i = 0; i < agenda.length; i++) {
              var a1 = {};
              a1.serverId = agenda[i]._id;
              //a1.confId = agenda[i].conf;
              a1.title = agenda[i].title;
              a1.abstractOfTalk = agenda[i].abstractOfTalk;
              a1.venue = agenda[i].venue;
              a1.startTime = moment(agenda[i].startTime).format("MMM DD, YYYY hh:mm:ss A");
              a1.duration = agenda[i].duration;

              a1.people = [];
              var p1 = {};
              for (var j = 0; j < agenda[i].speaker.length; j++) {
                p1.serverId = agenda[i].speaker[j]._id;
                //p1.agendaId = agenda[i].speaker[j].agendaId;
                //p1.confId = agenda[i].speaker[j].confId;
                p1.speakerName = agenda[i].speaker[j].speakerName;
                p1.type = agenda[i].speaker[j].type;
                p1.photo = agenda[i].speaker[j].photo;
                p1.tagLine = agenda[i].speaker[j].tagLine;
                p1.bio = agenda[i].speaker[j].bio;
                //  console.log(p1);
                a1.people.push(p1);

              }
              c.agendas.push(a1);
              console.log("Agenda added : ", a1);      
            }
            console.log("-----",c);
          }
          res.send(c);
        });
    } else {
      res.status(404);
      next();
    }
  });

};