var Conference = require('../models/Conference');
var Agenda = require('../models/Agenda');
var People = require('../models/People');
var mongoose = require('mongoose');
var moment = require('moment');
var Attendy = require('../models/Attendy');
var csv = require('csv');
exports.getDashboard = function (req, res, next)
{
   Conference.find({creator: req.user.email}, function (err, confs){
    if(err)
    {
      return next(err);
      
    }
    else{
    
        res.render('dashboard', { conferences:confs });
    }return;              
  });

};

exports.deleteConference = function(req,res,next)
{
  Conference.findById(req.params.id, function(err, result){
    
    if(result)
    {
      Agenda.find({confId:req.params.id},function(err, agendas){
      
        if(agendas)
        {
          Agenda.find({confId:req.params.id}).remove().exec();
          People.find({confId:req.params.id}).remove().exec();
        }
          
      });
        Conference.findById(req.params.id).remove().exec();
        
    }
  });
 
};
exports.downloadAttendyCSVFile = function(req, res, next)
{
  Attendy.find({confId: req.params.confId, attending: true }, function(err, result)
  {
    if(err)
    {
      return next(err);
    }
    if(result)
    {
      var data;
      var i=0;
      var attendy;
      while(i<result.length)
      {
        attendy= result[i];
        data =  attendy.name+ ", " + attendy.emailId +"\n";
        i++;
      
      }
      res.attachment('Attendy.csv');
      res.setHeader('Content-Type', 'text/csv');
      res.end(data);
    }else {
      console.error("Attendy not found.");
      res.status(404);
      next();
    }
             
  });

}