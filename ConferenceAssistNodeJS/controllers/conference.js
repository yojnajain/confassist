var Conf = require('../models/Conference');
var  fs = require("fs");
var validImageExtension = ['jpg', 'jpeg', 'gif', 'png'];
/**
 * GET /
 * create page.
 */

exports.getCreate = function (req, res) {
  res.render('conference', {
    conf: {},
    isCreate: true
  });
};

/**
 * POST /create
 * Retrieving data
 */

exports.postCreate = function (req, res, next) {

  req.assert('title', 'Title is required').notEmpty();
  req.assert('shortCode', 'ShortCode is required and should be unique for your conference.').notEmpty();
  req.assert('description', 'Description is required').notEmpty();
  req.assert('startDateEpoch', 'Start date is required for your conference').notEmpty();
  req.assert('endDateEpoch', 'End date is required for your conference').notEmpty();

  var conf = new Conf();

  conf.creator = req.user.email;
  conf.title = req.body.title.trim();
  conf.shortCode = req.body.shortCode.trim();
  conf.description = req.body.description.trim();
  conf.logo = req.body.logo.trim();
  conf.venue = req.body.venue.trim();
  conf.city = req.body.city.trim();
  conf.state = req.body.state.trim();
  conf.country = req.body.country.trim();
  //conf.latitude = req.body.latitude.trim();
  //conf.longitude = req.body.longitude.trim();
  conf.website = req.body.website.trim();
  conf.email = req.body.email.trim();
  conf.phoneNo = req.body.phoneNo.trim();
  if(req.body.startDateEpoch)
    conf.startDate = new Date(+req.body.startDateEpoch);
  if(req.body.endDateEpoch)
    conf.endDate = new Date(+req.body.endDateEpoch);

  
  /* if (req.files && req.files.logo && req.files.logo.size) {
    var tmp_path = req.files.logo.path;
    var ext = req.files.logo.name.split('.').pop().toLowerCase();
    if (validImageExtension.indexOf(ext) < 0) {
      fs.unlink(tmp_path);
      errors = {
        msg: 'Uploaded logo does not look like an image. Allowed files types are: ' + validImageExtension.join(' / ')
      };
    } else {
      var newFileName = Math.random().toString(36).slice(2) + '.' + ext;
      var target_path = './public/ext-media/' + newFileName;
      //conf.logo = '/ext-media/' + newFileName;
      // move the file from the temporary location to the intended location
      fs.rename(tmp_path, target_path, function (errFile) {
        if (errFile)
          console.error(errFile);
        fs.unlink(tmp_path);
      });
    }
  }*/
    
  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.render('conference', {
      conf: conf,
      isCreate: true
    });
  }

  conf.save(function (err, result) {
    req.flash('success', {
      msg: 'Your conference is published.'
    });
    res.redirect('/conference/' + result._id);
  });
};

/**
 * GET /
 * Edit Conference page.
 */
exports.getEdit = function (req, res) {
  Conf.findById(req.params.id, function (err, conference) {
    if (err) {
      console.error('Error in fetching conference: ' + err);
      return next(err);
    } else if (conference) {
      res.render('conference', {
        conf: conference,
      });
    } else {
      res.status(404);
      return next();
    }
  });
}


/**
 * POST /create
 * Retrieving data
 */
exports.postEdit = function (req, res, next) {
  Conf.findById(req.params.id, function (err, conf) {
    if (err) return next(err);

    req.assert('title', 'Title is required').notEmpty();
    req.assert('shortCode', 'ShortCode is required and should be unique for your conference.').notEmpty();
    req.assert('description', 'Description is required').notEmpty();
    req.assert('startDateEpoch', 'Start date is required for your conference').notEmpty();
    req.assert('endDateEpoch', 'End date is required for your conference').notEmpty();

    conf.title = req.body.title.trim();
    conf.shortCode = req.body.shortCode.trim();
    conf.description = req.body.description.trim();
   conf.logo = req.body.logo.trim();
    conf.venue = req.body.venue.trim();
    conf.city = req.body.city.trim();
    conf.state = req.body.state.trim();
    conf.country = req.body.country.trim();
    //conf.latitude = req.body.latitude.trim();
    //conf.longitude = req.body.longitude.trim();
    conf.website = req.body.website.trim();
    conf.email = req.body.email.trim();
    conf.phoneNo = req.body.phoneNo.trim();
    conf.startDate = new Date(+req.body.startDateEpoch);
    conf.endDate = new Date(+req.body.endDateEpoch);
    //conf.isAgendaFinal = req.body.isAgendaFinal.trim();

    var errors = req.validationErrors();

    if (errors) {
      req.flash('errors', errors);
      res.render('conference', {
        conf: conf,
      });
      return;
    }

    conf.save(function (err) {
      req.flash('success', {
        msg: 'Conference is updated.'
      });
      res.redirect('/conference/' + req.params.id);
    });
  });
};