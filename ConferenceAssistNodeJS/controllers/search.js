var Conf = require('../models/Conference');

exports.get = function (req, res, next) {
  
  
  if (!req.query.q)
    return res.redirect('/');
  
  Conf.textSearch(req.query.q, function (err, output) {
    if (err)
      return next(err);

    var foundConfes = output.results.map(function (eachResult) {
      return eachResult.obj;
    });
    var inspect = require('util').inspect;
    console.log(inspect(output, {
      depth: null
    }));
    res.render('search', {
      conferences: foundConfes,
      query: req.query.q
    });
  });

};