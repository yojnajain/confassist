/**
 * GET /
 * event page.
 */

var Conf = require('../models/Conference');
var Agenda = require('../models/Agenda');
var People = require('../models/People');
var Attendy = require('../models/Attendy');

/* GET Event Details. */
exports.getEvent = function (req, res, next) {

  Conf.findById(req.params.id, function (err, ConfDocs) {

    if (err) {
      console.error('Error in fetching conference: ' + err);
      next(err);
      return;
    } else if (ConfDocs) {
      Agenda.find({
        conf: req.params.id
      }).sort({
        startTime: 1
      }).exec(function (err, AgdDocs) {
        People.count({
          confId: req.params.id
        }, function (err, result) {
          if (result < 5)
            result = 0;
          else
            result = result - 5;
          People.find({
            confId: req.params.id
          })
            .limit(5).skip(Math.floor(Math.random() * (result)))
            .exec(function (err, pplDocs) {
              if (err) {
                console.error('Error in fetching People: ' + err);
                next(err);
                return;
              } else {
                Attendy.find({confId: req.params.id}, function(err, attendy)
                {
                    if(attendy)
                    {
                        res.render('event', {
                        conf: ConfDocs,
                        agendaList: AgdDocs,
                        peopleList: shuffle(pplDocs),
                        attendy: attendy.length
                        });  
                    }
                    else
                    {
                      res.render('event', {
                        conf: ConfDocs,
                        agendaList: AgdDocs,
                        peopleList: shuffle(pplDocs)
                      });
                    }
                });
                
              }
            });
        });
      });
    } else {
      res.status(404);
      return next();
    }
  });
};

function shuffle(o) {
  for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
};

exports.postDeleteConference = function (req, res, next) {
  Conf.findOne({
    _id: req.params.id
  }).exec(function (err, conference) {
    if (conference && req.user && conference.creator === req.user.email) {
      Agenda.find({
        conf: req.params.id
      }).remove().exec();
      People.find({
        confId: req.params.id
      }).remove().exec();
      Conf.find({
        _id: req.params.id
      }).remove().exec();
      req.flash('success', {
        msg: 'Conference has been deleted.'
      });
      return res.redirect('/');
    } else {
      res.status(404);
      return next();
    }
  });

};