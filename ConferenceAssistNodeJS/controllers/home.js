/**
 * GET /
 * Home page.
 */


var Conf = require('../models/Conference');

exports.get = function (req, res, next) {
  Conf.find({}, function (err, docs) {
    if (err)
      return next(err);
    else
      res.render('home', {
        conferences: docs
      });
    return;
  });
};