"use strict"
// Work Left
// 1. I want to insert agenda ID in people record.
var Conference = require('../models/Conference');
var Agenda = require('../models/Agenda');
var People = require('../models/People');
var mongoose = require('mongoose');
var moment = require('moment');
var Feedback = require('../models/Feedback');
var request = require('request');

exports.displayAgendaDetail = function (req, res, next) {

  Agenda.findOne({
    _id: req.params.id
  }).populate('speaker', undefined, 'people')
    .populate('conf', undefined, 'conf')
    .exec(function (err, agenda) {
      if (err) {
        console.log("Error while fetchings agenda", err);
        return next(err);
      }
      if (agenda) {
        var canEdit = req.user ? agenda.conf.creator == req.user.email : false;
        Feedback.find({
          agendaId: agenda._id
        }, function (err, feedbacks) {
          if (err) {
            console.log("Feedback error");
            return next(err);
          }
          res.render('agenda', {
            conf: agenda.conf,
            agenda: agenda,
            canEdit: canEdit,
            feedbacks: feedbacks
          });
          return;
        });
      } else {
        res.status(404);
        next();
      }
    });
};

//------- create agenda--------- 

exports.getCreate = function (req, res, next) {
  Conference.findById(req.params.id, function (err, conf) {
    if (err) {
      return next(err);
    }

    if (conf) {
      if (conf.creator == req.user.email) {

        res.render('agenda_create', {
          conf: conf,
          agenda: {
            speaker: [{}]
          },
          isEdit: false
        });
        return;
      } else {
        req.flash("Not Successfull", {
          msg: "Only authorized person can edit Talk"
        });
        res.status(404);
        return;
      }
    } else {
      res.status(404);
      next();
    }
  });
};

exports.postCreate = function (req, res, next) {


  req.assert("title", "Title should have atleast 4 characters and not more than 50.").isLength(4, 50);
  req.assert("abstractOfTalk", "Abstract should have atleast 20 characters and not more than 1000.").isLength(20, 1000);
  req.assert("venue", "Venue should have atleast 5 characters, maximum 200 characters.").isLength(5, 200);
  req.assert("startTime", "Start time can not be null.").notEmpty();
  req.assert("duration", "Duration shoud be a valid number of minutes.").isInt();
  req.assert("speakerName", "Speaker name can not be null").notEmpty();
  if (req.body.tagLine)
    req.assert("tagLine", "Tag Line should have atleast 4 characters and not more than 70.").isLength(4, 70);
  if (req.body.bio)
    req.assert("bio", "Description should have atleast 10 characters and not more than 200.").isLength(10, 200);
  if (req.body.photo)
    req.assert("photo", "photo link should be a valid website link").isURL({
      require_protocol: true,
      protocols: ['http', 'https']
    });
  var errors = req.validationErrors();
  if (errors) {
    var agd = {
      title: req.body.title,
      abstractOfTalk: req.body.abstractOfTalk,
      venue: req.body.venue,
      startTime: req.body.startTime,
      duration: req.body.duration,
      speaker: [{
        speakerName: req.body.speakerName,
        type: req.body.type,
        tagLine: req.body.tagLine,
        bio: req.body.bio,
        photo: req.body.photo
      }]
    }
    Conference.findById(req.params.id, function (err, conf) {
      if (err) {
        console.error("problem in fetching conference", err);
        return next(err);

      } else {
        req.flash("errors", errors);
        res.render('agenda_create', {

          agenda: agd,
          conf: conf
        });
        return;
      }
    });

    return;

  }

  var people = new People();
  people.confId = req.params.id;
  people.speakerName = req.body.speakerName;
  people.tagLine = req.body.tagLine;
  people.photo = req.body.photo;
  people.type = "speaker";
  people.bio = req.body.bio;


  var agenda = new Agenda();


  agenda.conf = mongoose.Types.ObjectId(req.params.id);
  agenda.title = req.body.title.trim();
  agenda.abstractOfTalk = req.body.abstractOfTalk.trim();
  agenda.venue = req.body.venue.trim();
  agenda.startTime = moment(req.body.startTime, 'YYYY/MM/DD HH:mm:ss a');
  agenda.duration = +req.body.duration.trim();
  agenda.speaker.push(people);
  console.log("Title  " + req.body.bio + ' @ ' + req.body.companyName);

  agenda.save(function (err, result) {

    if (err) {
      console.error("Error in saving agenda " + err);
      req.flash("errors", {
        msg: "Error has occured while saving, please try again."
      });
      res.redirect('back');
    } else {
      people.save(function (err, res) {
        if (err) {
          console.log("Error", err);
          res.status(404);
          return;
        } else {

        }
      });

      console.log("Agenda ID", result._id);
      people.agendaId = result._id;
      people.save(function (err, re) {
        if (err) {
          console.log("Error", err);
          res.status(404);
          return;
        }

      });
      req.flash("success", {
        msg: "Saved successfully"
      });

      if (req.body.save) {
        return res.redirect('/conference/' + req.params.id);
      } else {
        return res.redirect('/create/' + req.params.id + '/agenda');
      }
    }
  });
};

//-------edit agenda-----------

exports.getEdit = function (req, res, next) {
  Agenda.findOne({
    _id: req.params.agendaId
  })
    .populate('speaker', undefined, 'people')
    .populate('conf', undefined, 'conf')
    .exec(function (err, agenda) {
      if (err) {
        console.error("Error in fetching agenda ", err);
        return next(err);
      }
      if (agenda) {

        // var urL = '/agenda/' + agd._id + '/edit';
        if (agenda.conf.creator == req.user.email) {
          res.render('agenda_create', {
            conf: agenda.conf,
            agenda: agenda,
            isEdit: true
          });
          return;
        } else {
          req.flash("Not Successfull", {
            msg: "Only authorized person can edit Talk"
          });
          res.status(404);
          return;
        }
      } else {
        res.status(404);
        return next();
      }

    });
};

exports.postEdit = function (req, res, next) {



  req.assert("title", "Title should have atleast 4 characters and not more than 50.").isLength(4, 50);
  req.assert("abstractOfTalk", "Abstract should have atleast 20 characters and not more than 1000.").isLength(20, 1000);
  req.assert("venue", "Venue should have atleast 5 characters, maximum 200 characters.").isLength(5, 200);
  req.assert("startTime", "Start time can not be null.").notEmpty();
  req.assert("duration", "Duration shoud be a valid number of minutes.").isInt();
  req.assert("speakerName", "Speaker name can not be null").notEmpty();
  if (req.body.tagLine)
    req.assert("tagLine", "Tag Line should have atleast 4 characters and not more than 70.").isLength(4, 70);
  if (req.body.bio)
    req.assert("bio", "Description should have atleast 10 characters and not more than 200.").isLength(10, 200);
  if (req.body.photo)
    req.assert("photo", "photo link should be a valid website link").isURL({
      require_protocol: true,
      protocols: ['http', 'https']
    });
  var errors = req.validationErrors();

  if (errors) {

    Agenda.findOne({
      _id: req.params.agendaId
    })
      .populate('speaker', undefined, 'people')
      .populate('conf', undefined, 'conf')
      .exec(function (err, agenda) {
        if (err) {
          console.error("error in fetching agenda", err);
          return next(err);
        }
        if (agenda) {
          agenda.title = req.body.title;
          agenda.abstractOfTalk = req.body.abstractOfTalk;
          agenda.venue = req.body.venue;
          agenda.startTime = req.body.startTime;
          agenda.duration = req.body.duration;
          agenda.speaker = [{
            speakerName: req.body.speakerName,
            tagLine: req.body.tagLine,
            bio: req.body.bio,
            photo: req.body.photo
                }];

          req.flash("errors", errors);
          res.render('agenda_create', {
            agenda: agenda,
            conf: agenda.conf
          });
          return;

        } else {
          res.status(404);
          return next();
        }
      });

  } else {


    // -- saving data

    Agenda.findOne({
      _id: req.params.agendaId
    })
      .populate('speaker', undefined, 'people')
      .populate('conf', undefined, 'conf')
      .exec(function (err, agenda) {
        if (err) {
          console.error("error in fetching people data", err);
          return next(err);
        }
        if (agenda) {

          agenda.speaker[0].speakerName = req.body.speakerName;
          agenda.speaker[0].tagLine = req.body.tagLine;
          agenda.speaker[0].photo = req.body.photo;
          agenda.speaker[0].bio = req.body.bio;

          agenda.title = req.body.title.trim();
          agenda.abstractOfTalk = req.body.abstractOfTalk.trim();
          agenda.venue = req.body.venue.trim();
          agenda.startTime = moment(req.body.startTime, 'YYYY/MM/DD hh:mm:ss');
          agenda.duration = +req.body.duration.trim();


          agenda.save(function (err, result) {

            if (err) {
              console.error("Error in saving agenda " + err);
              req.flash("errors", {
                msg: "Error has occured while saving, please try again."
              });
              res.redirect('back');
              return;
            }
            if (result) {
              agenda.speaker[0].save(function (err, res) {
                if (err) {
                  console.log("Error", err);
                  res.status(404);
                } else {

                }
              });

              console.log("Agenda ID", result._id);

              req.flash("success", {
                msg: "Saved successfully"
              });

              return res.redirect('/agenda/' + agenda._id);

            } else {
              req.flash("Error", {
                msg: "Error occured in saving data"
              });

              return res.redirect('/agenda/' + agenda._id + '/edit');
            }
          });


        } else {
          res.status(404);
          return next();
        }

      });
  }
};

exports.postDeleteAgenda = function (req, res, next) {
  Agenda.findOne({
    _id: req.params.id
  }).populate('speaker', undefined, 'people').exec(function (err, agenda) {
    if (agenda) {
      Agenda.findOne({
        _id: req.params.id
      }).remove().exec();
      People.find({
        agendaId: req.params.id
      }).remove().exec();
      return res.redirect('/conference/' + agenda.conf);
    } else {
      return res.status(404);
    }
  });

};


exports.saveFeedback = function (req, res, next) {

  var feedback = new Feedback();
  //  feedback.userName = req.body.userName;
  feedback.emailId = req.query.email;
  feedback.agendaId = req.query.agendaId;
  feedback.feedback = req.query.feedback;
  feedback.rating = req.query.rating;
  feedback.createdDate = new Date();


  feedback.save(function (err, agd) {

    if (err) {
      console.error("Error in saving agenda " , err, req.query, feedback);
      req.flash("errors", {
        msg: "Error has occured while saving, please try again."
      });
      
      return res.status(404);
      
    } else {
      console.log(feedback);
      res.send(req.query);
    }
  });

};