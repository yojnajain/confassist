var mongoose = require('mongoose');
var People = require('./People');
var Conference = require('./Conference');

var AgendaSchema = new mongoose.Schema({

  conf: {type: mongoose.Schema.Types.ObjectId, ref : Conference},
  title: String,
  abstractOfTalk: String,
  venue: String,
  startTime: Date,
  duration: Number,
  speaker: [{type: mongoose.Schema.Types.ObjectId, ref : People}]
  
});

module.exports = mongoose.model('agenda',AgendaSchema);