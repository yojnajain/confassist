$(document).ready(function () {

  $('input[maxlength]').maxlength({
    alwaysShow: false,
    threshold: 40
  });
  $('textarea').maxlength({
    alwaysShow: true
  });
});